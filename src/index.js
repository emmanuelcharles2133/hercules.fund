import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { getLibrary } from './utils/web3React'
import { Web3Provider } from '@ethersproject/providers';
import { Web3ReactProvider } from '@web3-react/core';

// state management
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import WalletFeature from "./features/wallet";

const store = configureStore({
  reducer: {
    wallet: WalletFeature,
  },
});

ReactDOM.render(
  <React.StrictMode>
    <Web3ReactProvider getLibrary={getLibrary}>
      <Provider store={store}>
        {/* <Updaters /> */}
        <App />
      </Provider>
    </Web3ReactProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
