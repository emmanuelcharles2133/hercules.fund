import { React, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { ethers } from "ethers";

// wallet connect
import { useWeb3React } from "@web3-react/core";
import {
  injected,
  walletconnect,
  resetWalletConnector,
  walletlink,
} from "../Helpers/connectors";
import useSWR, { mutate } from "swr";
import { formatEther } from "@ethersproject/units";

// images
import menu from "./../../images/mobile-menu.png";
import logo from "./../../images/logoimg.svg";
import shape from "./../../images/Shape.svg";
import smile from "./../../images/smile.png";
import hercIco from "./../../images/herc-ico.png";
import laptop from "./../../images/computer.svg";
import trend from "./../../images/trend.svg";
import circIco from "./../../images/circ-ico.png";
import shuffleIco from "./../../images/shuffle-ico.png";
import bell from "./../../images/bell.png";
import shovelIco from "./../../images/shovel-ico.png";
import hercLogoIco from "./../../images/herc-logo-ico.png";
import arrow from "./../../images/arrow.png";
import bitpay from "./../../images/bitpay.png";
import metamask from "./../../images/metamask.png";
import argent from "./../../images/argent.png";
import rainbow from "./../../images/rainbow.png";
import pillar from "./../../images/pillar.png";
import gnosis from "./../../images/gnosis.png";
import blacto from "./../../images/blacto.png";
import walletConnect from "./../../images/wallet-connect.png";
import Defi from "./../../images/Defi.png";
import trustWallet from "./../../images/trustwallet.png";

import "./Header.css";
import UserDetails from "./../UserDetails/UserDetails";


const Header = ({setBal, setStakeInfo}) => {
  const [isActive, setActive] = useState(false);
  const [isActive2, setActive2] = useState(false);
  const [isActiveMobile, setActiveMobile] = useState(false);

  const [signInModal, setSignInModal] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [userAccount, setUserAccount] = useState(null);
  const [userBalance, setUserBalance] = useState(null);

  const handleToggle = () => {
    setActive2(false);
    setActive(!isActive);
  };

  const handleToggle2 = () => {
    setActive(false);
    setActive2(!isActive2);
  };

  const handleToggleMobile = () => {
    setActiveMobile(!isActiveMobile);
  };

  useEffect(() => {
    const account = localStorage.getItem("account");
    const balance = localStorage.getItem("balance");
    if (account) {
      try {
        setUserAccount(JSON.parse(account));
      } catch (err) {
        setUserAccount("Error Retrieving details");
      }
    }
    if (balance) {
      try {
        setUserBalance(balance);
      } catch (err) {
        setUserBalance("Error Retrieving details");
      }
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("account", JSON.stringify(userAccount));
    localStorage.setItem("balance", userBalance);
  });

  const handleDisconnect = () => {
    localStorage.removeItem("account");
    localStorage.removeItem("balance");
    alert("wallet disconnected");
    window.location.reload();
  };

    // Wallet Connect

    const web3reactContext = useWeb3React();
    const { active, account, library, connector, activate, deactivate } = web3reactContext

  // const connectWalletHandler = async () => {
  //   if (window.ethereum) {
  //     await window.ethereum
  //       .request({ method: "eth_requestAccounts" })
  //       .then((result) => {
  //         accountChangedHandler(result[0]);
  //       });
  //     setSignInModal(false);
  //   } else {
  //     setErrorMessage("You need to install metamask first");
  //   }
  // };

  const connectWalletHandler = async () => {
    try {

      resetWalletConnector(injected);

      await web3reactContext.activate(injected);
      setSignInModal(false);
    } catch (ex) {
      console.log(ex);
    }
  };

  const accountChangedHandler = (newAccount) => {
    setUserAccount(newAccount);
    getUserBalance(newAccount.toString());
  };

  const getUserBalance = (address) => {
    window.ethereum
      .request({ method: "eth_getBalance", params: [address, "latest"] })
      .then((balance) => {
        setUserBalance(ethers.utils.formatEther(balance));
      });
  };

  const chainChangedHandler = () => {
    window.location.reload();
  };

  if (window.ethereum) {
    window.ethereum.on("accountsChanged", accountChangedHandler);
    window.ethereum.on("chainChanged", chainChangedHandler);
  }

  const handleSignIn = () => {
    setSignInModal(true);
  };

  const connectWalletConnect = async () => {
    try {
      resetWalletConnector(walletconnect);
      await web3reactContext.activate(walletconnect);
      setSignInModal(false);
    } catch (ex) {
      console.log(ex);
    }
  };

  // wallet connect fetch balance

  const fetcher =
    (library) =>
    (...args) => {
      const [method, ...params] = args;
      console.log(method, params);
      return library[method](...params);
    };

  const Balance = () => {
    const { account, library } = useWeb3React();
    const { data: balance } = useSWR(["getBalance", account, "latest"], {
      fetcher: fetcher(library),
    });

    useEffect(() => {
      // this will listen for changes on an Ethereum address
      console.log(`listening for blocks...`);
      if (library) {
        library.on("block", () => {
          console.log("update balance...");
          mutate(undefined, true);
        });
        // This will remove the listener when the component is unmounted
        return () => {
          library.removeAllListeners("block");
        };
      }
      // This will trigger the effect only on component mount
    }, []);

    if (!balance) {
      return <span>...</span>;
    }
    return <span>{parseFloat(formatEther(balance)).toPrecision(4)}</span>;
  };

  
  useEffect(() => {
    setBal(account)
    setStakeInfo()
    return
  }, [account])

  return (
    <div className="Header">
      <div
        className={signInModal ? "sign-in open" : "sign-in"}
        onClick={() => {
          setSignInModal(false);
        }}
      >
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <h2>SIGN IN WITH YOUR WALLET</h2>
          <p>
            SIGN IN WITH ONE OF THE AVAILABLE WALLET PROVIDERS OR <br /> CREATE
            A NEW WALLET. <a href="#">WHAT IS A WALLET?</a>
          </p>
          <section>
            <div>
              <ul>
                <li className="recommended" onClick={connectWalletHandler}>
                  <img src={metamask} alt="" />
                  <div>
                    <p className="text-green">Recommended</p>
                    <p>SIGN IN WITH METAMASK</p>
                  </div>
                </li>
                <li onClick={connectWalletConnect}>
                  <img src={walletConnect} alt="" />
                  Wallet Connect
                </li>
                <li>
                  <img src={blacto} alt="" />
                  BLACTO
                </li>
                <li>
                  <img src={rainbow} alt="" />
                  RAINBOW
                </li>
              </ul>
            </div>
            <div className="more">
              <ul>
                <li>
                  <img src={Defi} alt="" />
                  DEIF WALLET
                </li>
                <li>
                  <img src={gnosis} alt="" />
                  GNOSIS
                </li>
                <li>
                  <img src={bitpay} alt="" />
                  BITPAY
                </li>
                <li>
                  <img src={argent} alt="" />
                  ARGENT
                </li>
                <li>
                  <img src={trustWallet} alt="" />
                  TRUST WALLET
                </li>
                <li>
                  <img src={pillar} alt="" />
                  PILLAR
                </li>
              </ul>
            </div>
          </section>
          <h1 className="error">{errorMessage}</h1>
        </div>
      </div>
      <div>
        <Link to ="/">
        <img src={logo} alt="Logo"/>
        </Link>
      </div>
      <ul className="desktop">
        <li>
          <Link to="/stack">
            <img src={shape} alt="" />
            STACK
          </Link>
        </li>
        <li className={isActive ? "dropdown active" : "dropdown"}>
          <button onClick={handleToggle} onBlur={handleToggle}>
            PRODUCTS
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
          </button>
          <div className="drop-menu" onMouseDown={(e)=>{e.preventDefault()}}>
            <ul className={isActive ? "dropdown-show" : null}>
              <img src={arrow} alt="" className="pointer" />
              <li>
                <Link to="/vault">
                  <img src={hercIco} alt="" /> VOTES
                </Link>
              </li>
              <li>
                <Link to="/swap">
                  <img src={shuffleIco} alt="" /> SWAP
                </Link>
              </li>
              <li>
                <Link to="/freeze">
                  <img src={bell} alt="" /> FREEZER
                </Link>
              </li>
              <li>
                <Link to="/portfolio">
                  <img src={circIco} alt="" /> PORTFOLIO
                </Link>
              </li>
              <li>
                <Link to="/farm">
                  <img src={shovelIco} alt="" /> FARM
                </Link>
              </li>
              <li>
                <Link to="/hcf-token">
                  <img src={hercLogoIco} alt="" /> HCF
                </Link>
              </li>
            </ul>
          </div>
        </li>

        <li className={isActive2 ? "dropdown2 active" : "dropdown2"}>
          <button onClick={handleToggle2} onBlur={handleToggle2}>
            GOVERNANCE
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
          </button>
          <div className="drop-menu2" onMouseDown={(e)=>{e.preventDefault()}}>
            <ul className={isActive2 ? "dropdown-show" : null}>
              <img src={arrow} alt="" className="pointer" />
              <li>
                <Link to="/stack">
                  <img src={hercLogoIco} alt="" /> HCF STAKING
                </Link>
              </li>
              <li>
                <Link to="/staking-simulator">
                  <img src={laptop} alt="" /> STAKING SIMULATOR
                </Link>
              </li>
              <li>
                <Link to="campaign">
                  <img src={trend} alt="" /> UMA KPI OPTIONS
                </Link>
              </li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#">
            <img src={smile} alt="smile" /> WHO WE ARE
          </a>
        </li>
        <li>
          <a href="#"> LEARN</a>
        </li>
        <li
          className={
            userAccount || web3reactContext.account
              ? "btn-gradient btn-green"
              : "btn-gradient"
          }
        >
          <button onClick={account ? handleDisconnect : handleSignIn}>
            {account
              ? "Disconnect Wallet"
              : "Connect Wallet"}
          </button>
        </li>
      </ul>
      <ul className="mobile-menu">
        <li><Link to="/stack"><img src={hercLogoIco} alt="" /> STAKE HCF </Link></li>
        <li onClick={handleToggleMobile} > <img src={menu} alt="" /></li>
        <div className={isActiveMobile ? "dropDownMobile-show" : "dropDownMobile"}>
          <section>
            <p>PRODUCTS:</p>
            <ul>
            <li>
                <Link to="/vault">
                  <img src={hercIco} alt="" /> VOTES
                </Link>
              </li>
              <li>
                <Link to="/swap">
                  <img src={shuffleIco} alt="" /> SWAP
                </Link>
              </li>
              <li>
                <Link to="/freeze">
                  <img src={bell} alt="" /> FREEZER
                </Link>
              </li>
              <li>
                <Link to="/portfolio">
                  <img src={circIco} alt="" /> PORTFOLIO
                </Link>
              </li>
              <li>
                <Link to="/farm">
                  <img src={shovelIco} alt="" /> FARM
                </Link>
              </li>
              <li>
                <Link to="/hcf-token">
                  <img src={hercLogoIco} alt="" /> HCF
                </Link>
              </li>
            </ul>
          </section>
          <section>
            <p>GOVERNANCE:</p>
            <ul>
            <li>
                <Link to="/stack">
                  <img src={hercLogoIco} alt="" /> HCF STAKING
                </Link>
              </li>
              <li>
                <Link to="/staking-simulator">
                  <img src={laptop} alt="" /> STAKING SIMULATOR
                </Link>
              </li>
              <li>
                <Link to="campaign">
                  <img src={trend} alt="" /> UMA KPI OPTIONS
                </Link>
              </li>
            </ul>
          </section>
          <section>
            <ul>
              <li>LEARN</li>
              <li>DOCS</li>
              <li className={
                  userAccount || web3reactContext.account
                    ? "btn-gradient btn-green"
                    : "btn-gradient"
                }>
                <button onClick={userAccount ? handleDisconnect : handleSignIn}>
                  {userAccount || web3reactContext.account
                    ? "Disconnect Wallet"
                    : "Connect Wallet"}
                </button>
              </li>
            </ul>
          </section>
        </div>
      </ul>
      {userAccount && (
        <UserDetails
          userAccount={userAccount}
          userBalance={userBalance}
          name={"Metamask"}
        />
      )}
      {web3reactContext.account && (
        <UserDetails
          userAccount={web3reactContext.account}
          userBalance={<Balance />}
          name={"Wallet Connect"}
        />
      )}
    </div>
  );
};

export default Header;
