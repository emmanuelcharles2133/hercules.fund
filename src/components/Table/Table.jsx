import Styles from "./Table.module.css";

const Table = ({ titles, children }) => {
  const condition = (k, n) => {
    if (k === n) {
      return true;
    }
  };

  const Header = () =>
    titles.map((item, k) => (
      <th
        key={k}
        className={`${condition(k, 1) && Styles.ml} ${
          condition(k, 2) && Styles.sML
        }`}
      >
        {item}
      </th>
    ));

  return (
    <table className={Styles.table}>
      {titles && (
        <tr className={Styles.titleSection}>
          <Header />
        </tr>
      )}
      <tr>{children}</tr>
    </table>
  );
};

export default Table;
