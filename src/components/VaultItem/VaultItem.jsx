import React, { useState, useEffect } from "react";
import styles from "./VaultItem.module.css";

const VaultItem = ({ data, color, mainIcon, icons }) => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    setIsMobile(window.innerWidth <= 480);
  }, []);

  return (
    <div className={styles.vaultItem}>
      {data.map((item) => (
        <>
          <p className={styles.name}>
            <img src={mainIcon} alt="" />
            <span>
              HCF
              {isMobile && (
                <p className={styles.mobileText}>
                  complete exposure to the Metaverse and NFTs. In collab with
                  NFTX
                </p>
              )}
            </span>
          </p>
          {!isMobile && (
            <>
              <p className={styles.assets}>
                {icons && icons.map((item) => <img src={item} alt="" />)}
              </p>
              <p className={styles.change} style={{ color: color }}>
                {item.change}
              </p>
              <p>{item.price}</p>
              <p className={styles.buy}>HCF</p>
            </>
          )}
          {isMobile && (
            <p className={styles.mobilePrice}>
              <p>{item.price}</p>
              <p className={styles.change} style={{ color: color }}>
                {item.change}
              </p>
            </p>
          )}
        </>
      ))}
    </div>
  );
};

export default VaultItem;
