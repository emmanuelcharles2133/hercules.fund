import Styles from "./VaultItem.module.css";

const SwapVaultItem = ({ images, swapTitle, details, left }) => {
  return (
    <div className={`${Styles.vaultItem} ${Styles.swapItem}`}>
      <p className={Styles.name}>
        {images && (
          <span className={Styles.swapImg}>
            {images.map((item, k) => (
              <img src={item} alt="" />
            ))}
          </span>
        )}
        <span>
            <p className={Styles.title}>{swapTitle}</p>
            <p className={Styles.details}>{details}</p>
        </span>
      </p>
      <h2 className={Styles.left}>{left}</h2>
    </div>
  );
};

export default SwapVaultItem;
