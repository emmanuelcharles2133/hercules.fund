import React from "react";

const BuyItemCard = ({
  itemData: { name, currentPrice, percentageIncrease, text, img, bolt }, buyNow
}) => {
  return (
    <div>
      <div className="item grad-text">
        <div className="top">
          {name} <br />
          <span>
            ${currentPrice} &nbsp;{" "}
            <span className="text-green">+{percentageIncrease}%</span>
          </span>
          <div className="hercImg">
            <img src={img} alt="" />
          </div>
        </div>
        <div className="middle">{text}</div>
      </div>
      {buyNow && (
        <div className="bottom">
          <div>
            <img src={bolt} alt="" /> since inception &nbsp;
            <span className="text-green">350.60%</span>
          </div>
          <span className="text-right">Buy now</span>
        </div>
      )}
    </div>
  );
};

export default BuyItemCard;
