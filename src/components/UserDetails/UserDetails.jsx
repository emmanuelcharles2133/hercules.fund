import React, { useState } from "react";
import "./userDetails.css";

// images
import user from "./../../images/herc-user.png";

const UserDetails = ({ userAccount, userBalance, name }) => {
  const [visible, setVisible] = useState(true);
  setTimeout(() => {
    setVisible(false);
  }, 3000);

  const availableAmount = "0";

  return (
    <div className={visible ? "userDetails show" : "userDetails"}>
      <div className="left">
        <img src={user} alt="" />
        <p>
          CONNECTED WITH <br /> {userAccount} <br /> <span> via {name} </span>
          <br /> BALANCE : {userBalance}
        </p>
      </div>
      <div className="right">
        <p>
          ${availableAmount} <br /> TO COLLECT
        </p>
        <button>START EARNING</button>
      </div>
    </div>
  );
};

export default UserDetails;
