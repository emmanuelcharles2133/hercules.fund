import React, { useState } from "react";
import { Link } from "react-router-dom";

// images
import logo from "./../../images/logoimg.svg";
import hercIco from "./../../images/herc-ico.png";
import laptop from "./../../images/computer.svg";
import trend from "./../../images/trend.svg";
import circIco from "./../../images/circ-ico.png";
import shuffleIco from "./../../images/shuffle-ico.png";
import bell from "./../../images/bell.png";
import shovelIco from "./../../images/shovel-ico.png";
import hercLogoIco from "./../../images/herc-logo-ico.png";

import "./Footer.css";

const Footer = () => {
  const [email, setEmail] = useState('');

  const handleInput = (e) => {
    e.preventDefault();
    setEmail(e.target.value.toLowerCase());
  }

  return (
    <div className="footer">
      <div className="desktop left">
        <div className="footer-nav">
          <div className="product">
            <ul>
              <li className="list-head">Products</li>
              <li>wallet</li>
              <li>Platform</li>
              <li>marketplace</li>
              <li>HCF</li>
              <li>Exchange</li>
            </ul>
          </div>
          <div className="governance">
            <ul>
              <li className="list-head">governance</li>
              <li>HCF stacking</li>
              <li>stacking simulator</li>
              <li>uma kpi options</li>
              <li>forum</li>
              <li>snapshot</li>
              <li>partnerships</li>
            </ul>
          </div>
          <div className="about">
            <ul>
              <li className="list-head">About</li>
              <li>company</li>
              <li>token standard</li>
              <li>careers</li>
              <li>contact us</li>
            </ul>
          </div>
        </div>
        <div className="footer-label">
          <img src={logo} alt="" />
          <p> &copy; 2022 hercules fund, All Rights Reserved.</p>
        </div>
      </div>
      <div className="right">
        <div>
          <h3>Get monthly updates on your inbox</h3>
          <input type="email" placeholder="ENTER YOUR EMAIL" value={email} onChange={handleInput} />
          <div>
            <p>We won’t spam you at all!</p>
            <button>SUBSCRIBE</button>
          </div>
        </div>
        <div className="footerMobile">
          <section>
            <p>PRODUCTS:</p>
            <ul>
            <li>
                <Link to="/vault">
                  <img src={hercIco} alt="" /> VOTES
                </Link>
              </li>
              <li>
                <Link to="/swap">
                  <img src={shuffleIco} alt="" /> SWAP
                </Link>
              </li>
              <li>
                <Link to="/freeze">
                  <img src={bell} alt="" /> FREEZER
                </Link>
              </li>
              <li>
                <Link to="/portfolio">
                  <img src={circIco} alt="" /> PORTFOLIO
                </Link>
              </li>
              <li>
                <Link to="/farm">
                  <img src={shovelIco} alt="" /> FARM
                </Link>
              </li>
              <li>
                <Link to="/hcf-token">
                  <img src={hercLogoIco} alt="" /> HCF
                </Link>
              </li>
            </ul>
          </section>
          <section>
            <p>GOVERNANCE:</p>
            <ul>
            <li>
                <Link to="/stack">
                  <img src={hercLogoIco} alt="" /> HCF STAKING
                </Link>
              </li>
              <li>
                <Link to="/staking-simulator">
                  <img src={laptop} alt="" /> STAKING SIMULATOR
                </Link>
              </li>
              <li>
                <Link to="campaign">
                  <img src={trend} alt="" /> UMA KPI OPTIONS
                </Link>
              </li>
            </ul>
          </section>
          <section>
            <ul>
              <li>LEARN</li>
              <li>DOCS</li>
              <li className=
              // {
              //     userAccount || web3reactContext.account
              //       ? "btn-gradient btn-green"
              //       : 
                    "btn-gradient"
                // }
                >
                <button 
                // onClick={userAccount ? handleDisconnect : handleSignIn}
                >
                  {/* {userAccount || web3reactContext.account
                    ? "Disconnect Wallet"
                    :  */}
                    "Connect Wallet"
                    {/* } */}
                </button>
              </li>
            </ul>
          </section>
        </div>
        <div className="desktop social-links">
          <ul>
            <li>discord</li>
            <li>telegram</li>
            <li>twitter</li>
            <li>forum</li>
            <li>docs</li>
            <li>github</li>
          </ul>
        </div>
      </div>
      
    </div>
  );
};

export default Footer;
