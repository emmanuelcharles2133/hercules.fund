import React, {useEffect, useState} from "react";
import Styles from "./Form.module.css";

const FormInput = ({
  id,
  value,
  type,
  disabled,
  setField,
  leftLabel,
  rightLabel,
  inputUnit,
  // dropdown,
  listed,
  callback,
  otherFunc
}) => {
  const [list, setList] = useState(id==="from"?0:3)
  const handleChange = (e) => {
    setField((prevState) => ({
      ...prevState,
      [id]: e.target.value,
    }));

    otherFunc && otherFunc(e.target.value)
  };
  const handleSelectChange = (e) => {
    setList(e.target.value)
  }

  useEffect(() => {
    if(listed) {
      callback(id, listed[list])
    }
  }, [list])

  return (
    <div className={Styles.card}>
      <div>
        {leftLabel && <h4>{leftLabel}</h4>}
        {rightLabel && <h4>{rightLabel}</h4>}
      </div>
      <section>
        <input
          id={id}
          disabled={disabled}
          value={value}
          onChange={handleChange}
          type={type || "number"}
          required
        />
        {inputUnit && <span className={Styles.inputUnit}>{inputUnit}</span>}
        {listed && !inputUnit && (
          <span className={Styles.inputUnit}>
            <select onChange={handleSelectChange} value={list}>
              {listed.map((item, index) => (
                <option key={id+item.symbol+index} value={index}>{item.symbol}</option>
              ))}
            </select>
          </span>
        )}
      </section>
    </div>
  );
};

export default FormInput;
