import React from "react";
import Styles from "./Form.module.css";

const FormExtra = ({ children }) => {
  return <div className={Styles.flex}>{children}</div>;
};

export default FormExtra;
