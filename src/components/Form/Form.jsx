import React from "react";
import Styles from "./Form.module.css";

const Form = ({ children, submit, submitText }) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    submit();
  };

  return (
    <div>
      <form onSubmit={handleSubmit} className={Styles.form}>
        {children}
        <button type="submit">{submitText}</button>
      </form>
    </div>
  );
};

export default Form;
