import React from "react";
import Styles from "./Modal.module.css";

const Modal = ({ showModal, closeModal, children }) => {
  return (
    <div>
      <div
        className={`${showModal && Styles.blurBg}`}
        onClick={() => closeModal(false)}
      />
      <div className={Styles.modal}>{children}</div>
    </div>
  );
};

export default Modal;
