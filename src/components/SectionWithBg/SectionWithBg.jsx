import Styles from "./SectionWithBg.module.css"

const SectionWithBg = ({ children}) => {
  return (
      <div className={Styles.container}>
          {children}
      </div>
  )
};

export default SectionWithBg;
