import Styles from "./Section.module.css";

const Section = ({ title, subTitle, titleIcon, children, childShadow }) => {
  return (
    <div className={Styles.container}>
      {title && (
        <div className={Styles.heading}>
          <h2>
            <img src={titleIcon} alt="" />
            <span>{title}</span>
          </h2>
          <p>{subTitle}</p>
        </div>
      )}
      <div className={childShadow && Styles.childShadow}>{children}</div>
    </div>
  );
};

export default Section;
