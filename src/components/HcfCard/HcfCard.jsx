import React from "react";
import "./HcfCard.css";

const HcfCard = ({ data, backgroundColor, icon, value }) => {
  return (
    <div
      className="hcfcard"
      style={{ backgroundColor: backgroundColor || "#E2E2E2" }}
    >
      <img src={icon} alt="" />
      {data.map((item, index) => (
        <section>
          <h4 className="title">{item.title}</h4>
          <h3 className="value">{value?value[index]:item.value} {item.unit?item.unit:''}</h3>
        </section>
      ))}
    </div>
  );
};

export default HcfCard;
