import Icon from "../../images/Sketch.png";
const HcfCardPortfolio = ({
  data,
  backgroundColor,
  icon,
  governanceReward,
  children,
}) => {
  return (
    <div
      className="hcfcard portfolio"
      style={{ backgroundColor: backgroundColor || "#E2E2E2" }}
    >
      {!children ? (
        <>
          <div className="flex">
            <img src={icon} alt="portfolio" />
            {data.map((item) => (
              <section>
                <h4 className="title">{item.title}</h4>
                <h3 className="value">{item.value}</h3>
              </section>
            ))}
          </div>
          {governanceReward && (
            <p className="reward">
              Earn Governance Rewards <img src={Icon} alt="Cream" />{" "}
            </p>
          )}
        </>
      ) : (
        <div>{children}</div>
      )}
    </div>
  );
};

export default HcfCardPortfolio;
