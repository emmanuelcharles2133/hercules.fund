import React from 'react';

// images

import pointer from "./../../images/arrow-right.png";
import hercLogo from "./../../images/herc-logo.png";

import styles from "./BuyHCF.module.css";

const BuyHCF = () => {
  return (
    <div className={styles.BuyHCF}>
      <a href="#">
        <div className={styles.img}>
            <div className={styles.goldGradient}>
            <img src={hercLogo} alt="" />
            </div>
            <div className={styles.buyHcf}>
            <h3>BUY HCF NOW</h3>
            <p>
                current price <span> $0.78 </span>
            </p>
            </div>
            <div className={styles.point}>
            <img src={pointer} alt="" />
            </div>
        </div>
    </a>
  </div>
  );
};

export default BuyHCF;
