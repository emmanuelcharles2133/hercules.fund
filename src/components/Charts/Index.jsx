import React from 'react'
import { ResponsiveContainer, AreaChart, CartesianGrid, XAxis, YAxis, Tooltip, Area } from "recharts"

const data = [
    {
        name: 'July 2020',
        uv: 4000,
        pv: 2400,
        amt: 2400,
      },
      {
        name: 'Aug 2020',
        uv: 3000,
        pv: 1398,
        amt: 2210,
      },
      {
        name: 'Sept 2020',
        uv: 2000,
        pv: 9800,
        amt: 2290,
      },
      {
        name: 'Oct 2020',
        uv: 2780,
        pv: 3908,
        amt: 2000,
      },
      {
        name: 'Nov 2020',
        uv: 1890,
        pv: 4800,
        amt: 2181,
      },
    {
      name: 'Dec 2020',
      uv: 4000,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'Jan 2021',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: 'Feb 2021',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: 'Mar 2021',
      uv: 2780,
      pv: 3908,
      amt: 2000,
    },
    {
      name: 'Apr 2021',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: 'May 2021',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: 'Jun 2021',
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
        name: 'July 2021',
        uv: 4000,
        pv: 2400,
        amt: 2400,
      },
      {
        name: 'Aug 2021',
        uv: 3000,
        pv: 1398,
        amt: 2210,
      },
      {
        name: 'Sept 2021',
        uv: 2000,
        pv: 9800,
        amt: 2290,
      },
      {
        name: 'Oct 2021',
        uv: 2780,
        pv: 3908,
        amt: 2000,
      },
      {
        name: 'Nov 2021',
        uv: 1890,
        pv: 4800,
        amt: 2181,
      },
      {
        name: 'Dec 2021',
        uv: 2390,
        pv: 3800,
        amt: 2500,
      },
      {
        name: 'Jan 2022',
        uv: 3490,
        pv: 4300,
        amt: 2100,
      },
  ];

const Index = () => {
    return (
        <div style={{height: "70vh", marginTop: "3rem", marginBottom: "3rem"}}>
                    <ResponsiveContainer width="100%" height="100%">
        <AreaChart
          width={500}
          height={400}
          data={data}
          margin={{
            top: 10,
            right: 30,
            left: 0,
            bottom: 0,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Area type="monotone" dataKey="uv" stroke="#8fe3f1" fill="#b2ecf5" />
        </AreaChart>
      </ResponsiveContainer>
        </div>
    )
}



export default Index
