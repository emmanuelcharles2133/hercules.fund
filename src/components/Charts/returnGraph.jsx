import React, {useEffect, useState} from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const returnGraph = () => {
    const data = [
        {
          name: '6 MONTHS',
          'HIGHEST APR': 0,
          'MEDIAN APR' : 0,
          'LOWEST APR': 0,
        },
        {
          name: '12 MONTHS',
          'HIGHEST APR': 7,
          'MEDIAN APR' : 6,
          'LOWEST APR': 5,
        },
        {
          name: '18 MONTHS',
          'HIGHEST APR': 12,
          'MEDIAN APR' : 11,
          'LOWEST APR': 10,
        },
        {
          name: '24 MONTHS',
          'HIGHEST APR': 17,
          'MEDIAN APR' : 16,
          'LOWEST APR': 15,
        },
        {
          name: '30 MONTHS',
          'HIGHEST APR': 22,
          'MEDIAN APR' : 21,
          'LOWEST APR': 20,
        },
        {
          name: '36 MONTHS',
          'HIGHEST APR': 35,
          'MEDIAN APR' : 32,
          'LOWEST APR': 31,
        },
      ];


  return (
    <ResponsiveContainer width="100%" height="100%">
        <LineChart
        width={500}
        height={300}
        data={data}
        margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
        }}
        >
      <CartesianGrid strokeDasharray="0 0" horzontal="" vertical=""/>
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line type="monotone" dataKey="LOWEST APR" stroke="#FFC700" activeDot={{r:5}}/>
      <Line type="monotone" dataKey="MEDIAN APR" stroke="#3CCCFF" activeDot={{r:5}}/>
      <Line type="monotone" dataKey="HIGHEST APR" stroke="#FD0079" activeDot={{r:5}}/>
    </LineChart>
  </ResponsiveContainer>
  );
};

export default returnGraph;
