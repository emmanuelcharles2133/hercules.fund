import { useReducer } from 'react'
import { ethers } from 'ethers';
import { useWeb3React } from "@web3-react/core";
import {
  // gettimeLockImpContract,
  // getHCFVoltContract,
  // getHCFTokenContract,
} from '../../utils/contractHelpers'
import { getClosest } from '../../utils/funcs';
import { simpleRpcProvider } from '../../utils/providers'
import { gettimeLockImpAddress } from '../../utils/addressHelpers';

const swapReducer = (state, action) => {
  switch (action.type) {
    case 'time_CHANGED': 
      return {
        ...state,
        time: action.payload
      };
    default:
      throw new Error();
  }
}

export const useSwapReducer = () => {
  const initialState = {
    time: 0
  };
  const [state, dispatch] = useReducer(swapReducer, initialState)

  const setTime = async (time_val) => {
    dispatch({type: 'time_CHANGED', payload: time_val})
  }

  return {
    swapState: state,
    setTime,
  }
}

