import { useReducer } from 'react'
import { ethers } from 'ethers';
import { useWeb3React } from "@web3-react/core";
import {
  gettimeLockImpContract,
  getHCFVoltContract,
  getHCFTokenContract,
} from '../../utils/contractHelpers'
import { getClosest } from '../../utils/funcs';
import { simpleRpcProvider } from '../../utils/providers'
import { gettimeLockImpAddress } from '../../utils/addressHelpers';

const baseReducer = (state, action) => {
  switch (action.type) {
    // case 'HCFVoltBal_CHANGED':
    //   return {
    //     ...state,
    //     hcfVolt_bal: action.payload
    //   };
    // case 'HCFTokenBal_CHANGED':
    //   return {
    //     ...state,
    //     hcf_bal: action.payload
    //   };
    case 'stakedData_CHANGED':
      return {
        ...state,
        staked_data: action.payload,
        totalStaked: ethers.utils.formatEther(action.payload.totalStaked),
        veTokenTotalSupply: ethers.utils.formatEther(action.payload.veTokenTotalSupply),
        accountVeTokenBalance: ethers.utils.formatEther(action.payload.accountVeTokenBalance),
        accountWithdrawableRewards: ethers.utils.formatEther(action.payload.accountWithdrawableRewards),
        accountWithdrawnRewards: ethers.utils.formatEther(action.payload.accountWithdrawnRewards),
        accountDepositTokenBalance: ethers.utils.formatEther(action.payload.accountDepositTokenBalance),
        accountDepositTokenAllowance: ethers.utils.formatEther(action.payload.accountDepositTokenAllowance),
        accountLocks: action.payload.accountLocks,
        accountAverage_month: action.payload.accountLocks.length?action.payload.accountLocks.reduce((a, b)=>parseFloat(a.lockDuration??0)+parseFloat(b.lockDuration??0), 0)/action.payload.accountLocks.length/2628000:0,
        voting_power: action.payload.veTokenTotalSupply>0?(action.payload.accountVeTokenBalance/action.payload.veTokenTotalSupply*100):0
      };
    // case 'HCFTokenStakedAmount_CHANGED':
    //   return {
    //     ...state,
    //     hcfToken_staked_amount: action.payload
    //   };
    // case 'HCFVoltSupply_CHANGED':
    //   return {
    //     ...state,
    //     hcfVolt_supply: action.payload
    //   };
    case 'HCFTokenSupply_CHANGED':
      return {
        ...state,
        hcfToken_supply: action.payload
      };
    case 'AverageMonth_CHANGED':
      return {
        ...state,
        average_month: action.payload
      };
    case 'tokenHolders_CHANGED':
      return {
        ...state,
        token_holders: action.payload
      };
    case 'minLockDuration_CHANGED': 
      return {
        ...state,
        min_lock_duration: action.payload
      };
    case 'maxLockDuration_CHANGED': 
      return {
        ...state,
        max_lock_duration: action.payload
      };
    case 'minLockAmount_CHANGED': 
      return {
        ...state,
        min_lock_amount: action.payload
      };
    default:
      throw new Error();
  }
}

const HCFVoltContract = getHCFVoltContract()
const HCFTokenContract = getHCFTokenContract()
const timeLockImpContract = gettimeLockImpContract()

async function getAccounts(fromBlock) {
  // const token = IERC20__factory.connect(tokenAddress, signer)
  // const filter = token.filters.Transfer(null, null, null);

  const BATCH_SIZE = 1000;
  const accounts = {};

  let currentFromBlock = fromBlock;
  const currentBlock = await simpleRpcProvider.getBlockNumber()
  console.log("current block", currentBlock);
  const filter = HCFTokenContract.filters.Transfer(null, null, null)
  while(true) {
    try {
      // TODO fix hitting limits of alchemy/infura
      console.log(`fetching ${currentFromBlock} to ${currentFromBlock + BATCH_SIZE}`)
      // const events = await token.queryFilter(filter, currentFromBlock, currentFromBlock + BATCH_SIZE);
      //   const events = await HCFTokenContract.getPastEvents('Transfer', {
      //     // filter: {myIndexedParam: [20,23], myOtherIndexedParam: '0x123456789...'}, // Using an array means OR: e.g. 20 or 23
      //     fromBlock: currentFromBlock,
      //     toBlock: currentFromBlock + BATCH_SIZE
      // }, function(error, events){ console.log(events); })
      const events = await HCFTokenContract.queryFilter(filter, currentFromBlock, currentFromBlock + BATCH_SIZE);
      console.log(events);
      for (const event of events) {
          if(event.args) {
              console.log("processing transfer");
              accounts[event.args._from] = {participation: 0};
              accounts[event.args._to] = {participation: 0};
          }
      }

      currentFromBlock += BATCH_SIZE;
      console.log(currentFromBlock);
      if(!currentBlock || currentFromBlock > currentBlock-BATCH_SIZE) {
          break;
      }
    } catch (error) {
      console.log(error);
      break
    }
  }
  console.log("end");
  console.log(accounts);
  return accounts;
}

export const useBaseReducer = () => {
  const initialState = {
    // hcfVolt_bal: 0,
    // hcf_bal: 0,
    // hcfToken_staked_amount: 0,
    hcfToken_supply: 0,
    // hcfVolt_supply: 0,
    veTokenTotalSupply: 0,
    totalStaked: 0,
    accountVeTokenBalance: 0,
    accountWithdrawableRewards: 0,
    accountWithdrawnRewards: 0,
    average_month: 0,
    accountAverage_month: 0,
    accountLocks: [],
    token_holders: [],
  };
  const [state, dispatch] = useReducer(baseReducer, initialState)

  // const { account } = useWeb3React()
  const setBal = async (account) => {
    console.log("account", account);
    console.log(account);
    // if(account){
    //   const hcfVoltBal = await HCFVoltContract.balanceOf(account)
    //   const hcfTokenBal = await HCFTokenContract.balanceOf(account)
    //   // const maxRatioArray = await HCFTokenContract.maxRatioArray()
    //   console.log("hcfVoltBal", hcfVoltBal.toNumber());
    //   dispatch({type: 'HCFVoltBal_CHANGED', payload: ethers.utils.formatEther(hcfVoltBal)})
    //   dispatch({type: 'HCFTokenBal_CHANGED', payload: ethers.utils.formatEther(hcfTokenBal)})
    // }
    // else{
    //   dispatch({type: 'HCFVoltBal_CHANGED', payload: 0})
    //   dispatch({type: 'HCFTokenBal_CHANGED', payload: 0})
    // }
    const stakedData = await timeLockImpContract.getStakingData(account??"0x0000000000000000000000000000000000000000")
    console.log("stakeddata", stakedData);
    console.log("stakeddata", account);
    console.log("stakeddata", account??"0x0000000000000000000000000000000000000000");
    dispatch({type: 'stakedData_CHANGED', payload: stakedData})
  }

  const setStakeInfo = async () => {
    const hcfVoltSupply = await HCFVoltContract.totalSupply()
    const hcfTokenSupply = await HCFTokenContract.totalSupply()
    const hcfTokenStakedAmount = await HCFTokenContract.balanceOf(gettimeLockImpAddress())
    // dispatch({type: 'HCFVoltSupply_CHANGED', payload: ethers.utils.formatEther(hcfVoltSupply)})
    dispatch({type: 'HCFTokenSupply_CHANGED', payload: ethers.utils.formatEther(hcfTokenSupply)})
    // dispatch({type: 'HCFTokenStakedAmount_CHANGED', payload: ethers.utils.formatEther(hcfTokenStakedAmount)})
    dispatch({type: 'AverageMonth_CHANGED', payload: hcfTokenStakedAmount?getClosest([...Array(36).keys()].map(value=>(value+1)/56.0268900276223*Math.log(value+1)), hcfVoltSupply/hcfTokenStakedAmount):0})
    const minLockDuration = await timeLockImpContract.minLockDuration()
    const maxLockDuration = await timeLockImpContract.maxLockDuration()
    const minLockAmount = await timeLockImpContract.minLockAmount()
    dispatch({type: 'minLockDuration_CHANGED', payload: minLockDuration})
    dispatch({type: 'maxLockDuration_CHANGED', payload: maxLockDuration})
    dispatch({type: 'minLockAmount_CHANGED', payload: ethers.utils.formatEther(minLockAmount)})
    
    const tokenHolders = await getAccounts(10090813);
    dispatch({type: 'tokenHolders_CHANGED', payload: tokenHolders})
  }

  return {
    stakeState: state,
    setBal,
    setStakeInfo,
  }
}

