import React, { useState, useEffect, useMemo, useReducer } from 'react'

import { isEmpty } from 'lodash'
import { useWeb3React } from '@web3-react/core'
import {useBaseReducer} from './reducer'

import {
  usetimeLockImpContract,
  useHCFVoltContract
} from "../../hooks/useContract"

import {
  gettimeLockImpContract,
  getHCFVoltContract
} from "../../utils/contractHelpers"

const intervalTime = 5000

export const BaseUpdater = ({setBal}) => {
  const [updatable, setUpdatable] = useState(0)
  // const { setBal } = useBaseReducer()
  const { account } = useWeb3React()
  useEffect(()=>{
    const interval = setInterval(() => {
      setUpdatable(prev => prev+1)
    }, intervalTime);
    setBal(account)
    return clearInterval(interval)
  }, [updatable])

  return null
}
