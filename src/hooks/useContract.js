import { useMemo } from 'react'
import useActiveWeb3React from './useActiveWeb3React'
// Addresses
import { 
  getHCFTokenAddress,
  getHCFVoltAddress,
  gettimeLockImpAddress
} from '../utils/addressHelpers'

import {
  gettimeLockImpContract,
  getHCFVoltContract
} from '../utils/contractHelpers'

import { getContract, getProviderOrSigner } from '../utils'
import ERC20_ABI from '../config/abi/erc20.json'
import timeLockImpABI from '../config/abi/timeLockImp.json'
import HCFVoltABI from '../config/abi/HCFVolt.json'

// Imports below migrated from Exchange useContract.ts
import { Contract } from '@ethersproject/contracts'

/**
 * Helper hooks to get specific contracts (by ABI)
 */

// export const usetimeLockImpContract = (address) => {
//   const { library } = useActiveWeb3React()
//   return useMemo(() => gettimeLockImpContract(address, library.getSigner()), [address, library])
// }

// Code below migrated from Exchange useContract.ts

// returns null on errors
function useContract(address, ABI, withSignerIfPossible = true) {
  const { library, account } = useActiveWeb3React()

  return useMemo(() => {
    if (!address || !ABI || !library) return null
    try {
      return getContract(address, ABI, library, withSignerIfPossible && account ? account : undefined)
    } catch (error) {
      console.error('Failed to get contract', error)
      return null
    }
  }, [address, ABI, library, withSignerIfPossible, account])
}

export function useTokenContract(tokenAddress, withSignerIfPossible) {
  return useContract(tokenAddress, ERC20_ABI, withSignerIfPossible)
}

export const usetimeLockImpContract = (address, withSignerIfPossible) => {
  return useContract(address, timeLockImpABI, withSignerIfPossible)
}

// export const useHCFVoltContract = (address = getHCFVoltAddress(), withSignerIfPossible) => {
//   console.log(address);
//   console.log(withSignerIfPossible);
//   return useContract(address, HCFVoltABI)
// }
export const useHCFVoltContract = (address = getHCFVoltAddress(), withSignerIfPossible) => {
  console.log(address);
  console.log(withSignerIfPossible);
  return useContract(address, HCFVoltABI)
}
