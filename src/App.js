import React from "react";
import { BrowserRouter as Router, Routes , Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import VaultPage from "./pages/Vault/Vault";
import StackPage from "./pages/StackPage/Stack";
import Freeze from "./pages/Freeze/Freeze";
import HCFIndex from "./pages/HCFIndex/HCFIndex";
import Swap from "./pages/Swap/Swap";
import Farm from "./pages/Farm/Farm";
import Portfolio from "./pages/Portfolio/Portfolio";
import HCFToken from "./pages/HCFToken/HCFToken";
import StakingSimulator from "./pages/StakingSimulator/StakingSimulator";

import { Web3Provider } from '@ethersproject/providers';
import Campaign from "./pages/Campaign/Campaign";
import { BaseUpdater } from './state/base/updater'
import {useBaseReducer} from './state/base/reducer'
import {useSwapReducer} from './state/swap/reducer'
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

const Updaters = ({setBal}) => {
  return (
    <>
      <BaseUpdater setBal={setBal}/>
    </>
  )
}

function App() {
  const { stakeState, setBal, setStakeInfo } = useBaseReducer()
  const { swapState, setTime } = useSwapReducer()

  const getLibrary = (provider) => {
		const library = new Web3Provider(provider, 'any');
		library.pollingInterval = 15000;
		return library;
	};

  return (
    <div className="body">
      {/* <Updaters setBal={setBal}/> */}
        <Router>
        <Header setBal={setBal} setStakeInfo={setStakeInfo} />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/stack" element={<StackPage state={stakeState} />} />
            <Route path="/vault" element={<VaultPage />} />
            <Route path="/freeze" element={<Freeze />} />
            <Route path="/hcf-index" element={<HCFIndex />} />
            <Route path="/swap" element={<Swap state={swapState} setTime={setTime} />} />
            <Route path="/farm" element={<Farm />} />
            <Route path="/portfolio" element={<Portfolio state={stakeState} />} />
            <Route path="/hcf-token" element={<HCFToken />} />
            <Route path="/staking-simulator" element={<StakingSimulator />} />
            <Route path="/campaign" element={<Campaign />} />
          </Routes>
        <Footer />
        </Router>
      </div>
  );
};

export default App;
