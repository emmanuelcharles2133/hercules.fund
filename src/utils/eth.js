import ERC20_ABI from '../config/abi/erc20.json';
import { ethers } from 'ethers';
import { getERC20Contract } from './contractHelpers';

export const approve = async (address, spender, amount, signer) => {
  // const erc20Contract = await contract({ address, abi: ERC20_ABI });
  const erc20Contract = getERC20Contract(address, signer)
  const { hash } = await erc20Contract.approve(spender, amount);
  return true;
};

// eslint-disable-next-line max-len
export const approveMax = async (address, spender, signer) => approve(address, spender, ethers.constants.MaxUint256, signer);
