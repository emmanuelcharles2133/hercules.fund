const dummyData = {
  stats: {
    1: [
      {
        title: "total staking hcf",
        unit: "hcf",
      },
      {
        title: "total circulatin hcf",
        value: "64.9% of 39.32m",
      },
    ],
    2: [
      {
        title: "total staking hercules",
        unit: "hcf",
      },
      {
        title: "total circulatin hcf",
        value: "64.9% of 39.32m",
      },
    ],
    3: [
      {
        title: "average time lock",
        unit: "months",
      },
    ],
    4: [
      {
        title: "voting addresses",
        value: "695",
      },
    ],
  },
  summary: {
    1: [
      {
        title: "your total stake hcf",
        unit: "hcf",
      },
    ],
    2: [
      {
        title: "your total staked hercules",
        value: "0 hercules",
      },
    ],
    3: [
      {
        title: "claimable rewards",
        unit: "hcf",
      },
    ],
    4: [
      {
        title: "average locking period",
        value: "695",
        unit: "months"
      },
    ],
    5: [
      {
        title: "your voting power",
        unit: "%",
      },
    ],
  },
    vaultItem: {
      1: [
        {
          change: "+2.45",
          price: "$5.9",
        },
      ],
      2: [
        {
          change: "+2.45",
          price: "$5.9",
        },
      ],
      3: [
        {
          change: "+2.45",
          price: "$5.9",
        },
      ],
      4: [
        {
          change: "-2.45",
          price: "$5.9",
        },
      ],
      5: [
        {
          change: "+2.45",
          price: "$5.9",
        },
      ],
      6: [
        {
          change: "+2.45",
          price: "$5.9",
        },
      ],
    }
};

export default dummyData;
