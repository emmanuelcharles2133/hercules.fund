import addresses from '../config/constants/contracts'
import "dotenv/config"

export const getAddress = (address) => {
  const chainId = process.env.REACT_APP_CHAIN_ID
  return address[chainId] ? address[chainId] : address[ChainId.MAINNET]
}

export const getPProxiedFactoryAddress = () => {
  return getAddress(addresses.PProxiedFactory)
}
export const getPV2SmartPoolAddress = () => {
  return getAddress(addresses.PV2SmartPool)
}
export const getRecipeAddress = () => {
  return getAddress(addresses.Recipe)
}
export const getOvenFactoryContractAddress = () => {
  return getAddress(addresses.OvenFactoryContract)
}
export const getPTOvenAddress = () => {
  return getAddress(addresses.PTOven)
}
export const getRegistryAllAddress = () => {
  return getAddress(addresses.RegistryAll)
}
export const getdiamondCutFacetAddress = () => {
  return getAddress(addresses.diamondCutFacet)
}
export const getdiamondLoupeFacetAddress = () => {
  return getAddress(addresses.diamondLoupeFacet)
}
export const getdiamondImplementationAddress = () => {
  return getAddress(addresses.diamondImplementation)
}
export const gettimeLockImpAddress = () => {
  return getAddress(addresses.timeLockImp)
}
export const getHCFVoltAddress = () => {
  return getAddress(addresses.HCFVolt)
}
export const getHCFTokenAddress = () => {
  return getAddress(addresses.HCFToken)
}

