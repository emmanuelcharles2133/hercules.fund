import { ethers } from 'ethers'
import { simpleRpcProvider } from './providers'
// import tokens from '../config/constants/tokens'

// Addresses
import { 
  getHCFTokenAddress,
  getHCFVoltAddress,
  gettimeLockImpAddress
} from './addressHelpers'

// ABI
import timeLockImpABI from '../config/abi/timeLockImp.json'
import HCFVoltABI from '../config/abi/HCFVolt.json'
import HCFTokenABI from '../config/abi/HCFToken.json'
import ERC20_ABI from '../config/abi/erc20.json'

const getContract = (abi, address, signer) => {
  console.log("simpleRpcProvider", simpleRpcProvider);
  const signerOrProvider = signer ?? simpleRpcProvider
  return new ethers.Contract(address, abi, signerOrProvider)
}

export const gettimeLockImpContract = (signer) => {
  return getContract(timeLockImpABI, gettimeLockImpAddress(), signer)
}

export const getHCFVoltContract = (signer) => {
  return getContract(HCFVoltABI, getHCFVoltAddress(), signer)
}

export const getHCFTokenContract = (signer) => {
  return getContract(HCFTokenABI, getHCFTokenAddress(), signer)
}

export const getERC20Contract = (address, signer) => {
  return getContract(ERC20_ABI, address, signer)
}
