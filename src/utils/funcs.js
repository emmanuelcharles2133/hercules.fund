import { ethers } from "ethers";

export const getClosest = (array, goal) => {
  const closest = array.reduce(function(prev, curr) {
    // const prev = ethers.utils.formatEther(prev0)
    // const curr = ethers.utils.formatEther(curr0)
    return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
  });
  return array.indexOf(closest)
}