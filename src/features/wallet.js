import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  address: "",
};

const userSlice = createSlice({
  name: "wallet",
  initialState: initialState,
  reducers: {
    login: (state) => {
      state.address = "";
    },
  },
});

export const { login } = userSlice.actions;
export default userSlice.reducer;