import React from "react";

import hercules from "./../../images/hercules.png";
import bitpay from "./../../images/bitpay.png";
import metamask from "./../../images/metamask.png";
import rainbow from "./../../images/rainbow.png";
import argent from "./../../images/argent.png";
import pillar from "./../../images/pillar.png";
import gnosis from "./../../images/gnosis.png";
import Defi from "./../../images/Defi.png";
import trustWallet from "./../../images/trustwallet.png";
import hercLogo from "./../../images/herc-logo.png";
import hercRubix from "./../../images/herc-rubix.png";
import arrowRight from "./../../images/arrow-right-solid.svg";
import herculesHeroImg from "./../../images/hercules-heroimg.png";
import pointer from "./../../images/arrow-right.png";
import image from "./../../images/HCF-img.png";
import bolt from "./../../images/bolt.png";
import purpletick from "./../../images/purpletick.svg";

import BuyItemCard from "../../components/Cards/BuyItemCard";

import "./Home.css";

const Home = () => {
  const itemData = {
    name: "PLAY",
    currentPrice: "3.62",
    percentageIncrease: "2.06",
    text: "COMPLETE EXPOSURE TO THE METAVERSE AND NFTS. IN COLLAB WITH NFTX",
    img: hercLogo,
    bolt: bolt,
  };

  return (
    <div>
      <div className="hero">
        <div className="hero-main">
          <div className="left">
            <div className="main-text">
              <h2>
                The DAO's
                <br />
                is more
                <br />
                near than ever
                <br />
              </h2>
              <h4>
                The best solutions came from
                <br />
                the best people. Be part of this
                <br />
                community
              </h4>
            </div>
            <a href="#">
              <div className="img">
                <div className="gold-gradient">
                  <img src={hercLogo} alt="" />
                </div>
                <div className="buy-hcf">
                  <h3>BUY HCF NOW</h3>
                  <p>
                    current price <span> $0.78 </span>
                  </p>
                </div>
                <div className="point">
                  <img src={pointer} alt="" />
                </div>
              </div>
            </a>
          </div>
          <div className="right">
            <img
              src={herculesHeroImg}
              className="hercules-hero-img"
              alt="Hero"
            />
            <img src={hercules} className="hercules-img" alt="Hero" />
          </div>
        </div>
      </div>
      <div className="sec2">
        <div className="gradient-border">
          <BuyItemCard itemData={itemData} buyNow />
        </div>

        <div className="gradient-border">
          <BuyItemCard itemData={itemData} buyNow />
        </div>

        <div className="gradient-border">
          <BuyItemCard itemData={itemData} buyNow />
        </div>

        <div className="gradient-border">
          <BuyItemCard itemData={itemData} buyNow />
        </div>

        <div className="gradient-border">
          <BuyItemCard itemData={itemData} buyNow />
        </div>

        <div className="gradient-border">
          <BuyItemCard itemData={itemData} buyNow />
        </div>

        <div className="browse-all">
          BROWSE ALL <img src={arrowRight} alt="" />
        </div>
      </div>
      <div className="sec3">
        <h2>
          Hercules Fund is <br /> the Earning Place
          <span>
            Receive good earrings with our token plans. This is a growing
            platform with a huge investment <br /> community.
          </span>
          <span className="wallet">Connect Wallet</span>
        </h2>
      </div>
      <div className="sec4">
        <div className="left-text">
          <h2>Token means power</h2>
          <br />
          <p>
            With our HCF token, you can vote and be part of a big decentralized
            family. Your decisions counts and you can choose what is the best
            for all. Everyone has the chance to do something and make great
            changes right now.
            <br />
            <br />
            Here is a place to share a talk with no limits, you can achieve
            goals and earn investing in our tokens. Every ERC-20 got a little
            piece of power.
            <br />
            <br />
            Don't waist it and let's join to this community. We are waiting for
            you
          </p>
        </div>
        <img src={image} alt="" />
      </div>
      <div className="sec5">
        <div className="bg-black">
          <h5>
            Token <br />
            Powers <br />
            (HCF)
          </h5>
        </div>

        <div className="bg-orange">
          <h5>
            Good <br />
            earrings <br />
            distribution
          </h5>
        </div>

        <div className="bg-purple">
          <h5>
            The Best <br />
            DAO's <br />
            Platform
          </h5>
        </div>

        <div className="bg-pink">
          <h5>
            Your <br />
            decisions <br />
            values
          </h5>
        </div>

        <div className="bg-blue">
          <h5>
            Very huge <br />
            investment <br />
            community
          </h5>
        </div>
      </div>
      <div className="sec6">
        <img src={hercRubix} alt="" />
        <h5>
          You're a click of distance to start working with us. Your portfolio
          will be the best thing that you would have, and you can exchange for
          other crypto. Token values,
          <br /> remember that.
          <span>
            learn more <img src={arrowRight} alt="" />
          </span>
        </h5>
      </div>
      <div className="sec7">
        <h3>Our wallets</h3>
        <div className="wallets">
          <div>
            <img src={Defi} alt="" />
            <p>DEFI Wallet</p>
          </div>
          <div>
            <img src={gnosis} alt="" />
            <p>Gnosis</p>
          </div>
          <div>
            <img src={bitpay} alt="" />
            <p>bitpay</p>
          </div>
          <div>
            <img src={metamask} alt="" />
            <p>metamask</p>
          </div>
          <div>
            <img src={argent} alt="" />
            <p>argent</p>
          </div>
          <div>
            <img src={trustWallet} alt="" />
            <p>trust wallet</p>
          </div>
          <div>
            <img src={rainbow} alt="" />
            <p>rainbow</p>
          </div>
          <div>
            <img src={pillar} alt="" />
            <p>pillar</p>
          </div>
        </div>
      </div>
      <div className="sec8">
        <h3>Hercules Fund developed the best Smart Contracts for you</h3>
        <ul>
          <li>
            <div>
              <img src={purpletick} alt="" /> <span></span>
            </div>
            <p>Protected platform. No scams here.</p>
          </li>
          <li>
            <div>
              <img src={purpletick} alt="" /> <span></span>
            </div>
            <p>
              Audited Smart Contracts. We have Blockchain and Solidity experts
              in this field.
            </p>
          </li>
          <li>
            <div>
              <img src={purpletick} alt="" /> <span></span>
            </div>
            <p>Strong portoflio with good features.</p>
          </li>
          <li>
            <div>
              <img src={purpletick} alt="" />
            </div>
            <p>Fast exchange. Your receive your money fast.</p>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Home;
