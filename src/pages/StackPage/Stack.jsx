import React, { useEffect } from "react";
import HcfCard from "../../components/HcfCard/HcfCard";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import dummyData from "../../utils/dummyData";
import Form from "../../components/Form/Form"
import FormInput from "../../components/Form/FormInput"

import { ethers } from "ethers";

// Images
import metamask from "./../../images/metamask.png";
import rainbow from "./../../images/rainbow.png";
import pillar from "./../../images/pillar.png";
import chartLine from "./../../images/Chart-line.png";
import HCF from "./../../images/HCF.png";
import HCFONLYLOGO from "./../../images/HF-LOGO-ICON-ONLY.png";
import LockPng from "./../../images/Lock.png";
import Group from "./../../images/Group.png";
import Sketch from "./../../images/Sketch.png";
import File from "./../../images/File.png";

import "./Stack.css";

import Web3 from "web3-react"
import { useWeb3React } from "@web3-react/core";
import { useBaseReducer } from '../../state/base/reducer'
import {
  gettimeLockImpContract,
  getHCFVoltContract,
  getHCFTokenContract,
} from '../../utils/contractHelpers'
import { gettimeLockImpAddress } from "../../utils/addressHelpers";

const Stack = ({state}) => {
  const { account, library } = useWeb3React()
  
  const [{ amount, duration }, setStakeData] = React.useState({
    amount: null,
    duration: null,
  });
  const [durationInUnit, setUnitData] = React.useState("");
  const timeLockImpAddress = gettimeLockImpAddress()
  const handleSubmit = async () => {
    const HCFTokenContract = getHCFTokenContract(library.getSigner())
    if(ethers.utils.formatEther(await HCFTokenContract.allowance(account, timeLockImpAddress))<amount){
      await HCFTokenContract.approve(timeLockImpAddress, ethers.constants.MaxUint256)
    }
    const timeLockImpContract = gettimeLockImpContract(library.getSigner())
    
    try {
      await timeLockImpContract.depositByMonths(ethers.utils.parseEther(amount), duration, account)
    } catch (error) {
      console.log(error);
    }
  };

  const calculateDuration = (value) => {
      const unitInYears = parseInt(value) / 12;
      setUnitData(`${unitInYears || ""} Years`);
  }

  return (
    <div>
      <div>
        <div className="sec3">
          <h2>
            HCF STAKING
            <span>
              Receive good earrings with our token plans. This is a growing
              platform with a huge investment community.
            </span>
            <span className="wallet">Connect Wallet To Stake Now</span>
          </h2>
        </div>

        <div className="sec1">
          <h2 className="title">
            <img src={chartLine} alt="" className="title-icon" />
            HCF KEY STATS
          </h2>
          <div className="container">
            <h2>
              <span>
                The hcf token is a proof-of-stake token used to secure hercules
                Network's mainnet, called Basechain. hcf holders can
                stake their tokens to help secure Basechain and earn rewards in the process.
              </span>
            </h2>
            <div className="stats">
              <HcfCard
                data={dummyData.stats[1]}
                backgroundColor="#e3f9e6"
                icon={HCF}
                value={[state.totalStaked, state.hcfToken_supply?(state.totalStaked/state.hcfToken_supply).toFixed(5)*100:0]}
              />
              <HcfCard
                data={dummyData.stats[2]}
                backgroundColor="#F9E3F4"
                icon={HCFONLYLOGO}
                value={[state.totalStaked, state.hcfToken_supply?(state.totalStaked/state.hcfToken_supply).toFixed(5)*100:0]}
              />
              <HcfCard
                data={dummyData.stats[3]}
                backgroundColor="#E2E2E2"
                icon={LockPng}
                value={[state.average_month]}
              />
              <HcfCard
                data={dummyData.stats[4]}
                backgroundColor="#E2E2E2"
                icon={Group}
                value={[Object.keys(state.token_holders).length]}
              />
            </div>
          </div>
        </div>

        <div className="sec1 summary">
          <h2 className="title">
            <img src={File} alt="" className="title-icon" />
            Summary
          </h2>
          <div className="container">
            <h2>
              <span>
                The hcf token is a proof-of-stake token used to secure hercules
                Network's mainnet, called Basechain. hcf holders can
                stake their tokens to help secure Basechain and earn
                rewards in the process.
              </span>
            </h2>
            <div className="stats">
              <HcfCard data={dummyData.summary[1]} value={[state.accountLocks.length&&state.accountLocks.reduce((a, b)=>parseFloat(ethers.utils.formatEther(a.amount??0))+parseFloat(ethers.utils.formatEther(b.amount??0)), 0)]} icon={HCF}/>
              <HcfCard data={dummyData.summary[2]} value={[state.accountLocks.length&&state.accountLocks.reduce((a, b)=>parseFloat(ethers.utils.formatEther(a.amount??0))+parseFloat(ethers.utils.formatEther(b.amount??0)), 0)]} icon={HCFONLYLOGO} />
              <HcfCard data={dummyData.summary[3]} value={[state.accountWithdrawableRewards]} icon={Group} />
              <HcfCard data={dummyData.summary[4]} value={[state.accountAverage_month]} icon={LockPng} />
              <HcfCard data={dummyData.summary[5]} value={[state.voting_power]} icon={HCF} />
            </div>
            <div className="connect-wallet">
              <h2>
                <span>add hcf to meta mask or other waller</span>
              </h2>
              <div>
                <img src={metamask} alt="Metamask" />
                <img src={rainbow} alt="Rainbow" />
                <img src={pillar} alt="Pillar" />
                <a href="#">View all</a>
              </div>
            </div>
          </div>
        </div>

        <div className="sec1 stake-now">
          <h2 className="title">
            <img src={Sketch} alt="" className="title-icon" />
            Stake now
          </h2>
          <div className="container">
            <h2>
              <span>
                The hcf token is a proof-of-stake token used to secure hercules
                Network's mainnet, called Basechain. hcf holders can
                stake their tokens to help secure Basechain and earn rewards in the process.
              </span>
            </h2>
            
            <Form submit={handleSubmit} submitText="Connect Wallet To Stake Now">
              <FormInput
                id="amount"
                leftLabel="Amount To Stake"
                rightLabel="BALANCE: 0.0000 HCF"
                inputUnit="HCF"
                setField={setStakeData}
                value={amount}
              />
              <FormInput
                id="duration"
                leftLabel="STAKE DURATION (6 TO 36 MONTHS)"
                rightLabel="BALANCE: 0.0000 HCF"
                setField={setStakeData}
                inputUnit={durationInUnit}
                otherFunc={calculateDuration}
                value={duration}
              />
            </Form>

            <div className="connect-wallet">
              <h2>
                <span>add hcf to meta mask or other wallet</span>
              </h2>
              <div>
                <img src={metamask} alt="Metamask" />
                <img src={rainbow} alt="Rainbow" />
                <img src={pillar} alt="Pillar" />
                <a href="#">View all</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Stack;
