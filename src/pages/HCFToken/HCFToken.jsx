import Header from "../../components/Header/Header";
import Section from "../../components/Section/Section";
import HcfCard from "../../components/HcfCard/HcfCard";
import Footer from "../../components/Footer/Footer";
import Styles from "./HCFToken.module.css";
import dummyData from "../../utils/dummyData";

import HCFLogo from "../../images/HCF.png";
import RightIcon from "../../images/left.png";
import Hercules from "../../images/hercules-heroimg.png";
import MetaMask from "../../images/metamask.png";
import Rainbow from "../../images/rainbow.png";
import Pillar from "../../images/pillar.png";
import ChartIcon from "../../images/Chart-line.png";
import HCF from "../../images/HCF.png";
import HCFONLYLOGO from "../../images/HF-LOGO-ICON-ONLY.png";
import LockPng from "../../images/Lock.png";
import Group from "../../images/Group.png";
import herculesGif from "../../images/hercules.gif";
import BuyHCF from "../../images/buyHCF.png";
import Shield from "../../images/Shield-check.png";

const HCFToken = () => {
  const subTitle = (
    <p>
      This is how the HCF makes money and how is redistributing them to the
      system <br />
      <span className={Styles.yellow}>
        Test your assumptions on the simulator {">"}
      </span>
    </p>
  );

  const featuresText = [
    "Token Powers (HCF)",
    "Good earings distribution",
    "The Best DAO's Platform",
    "Your decisions values",
    "Very huge investment community",
  ];

  const FeaturesCard = ({ title }) => (
    <div className={Styles.FeaturesCard}>
      <h4>{title}</h4>
    </div>
  );

  const GovernanceCard = ({ address }) => (
    <div className={Styles.governanceCard}>
      <div className={Styles.header}>
        <p className={Styles.address}>
          {address || "0x0810C422E4abD05c752618B403d26cf61111111"}
        </p>
        <p className={Styles.status}>CLOSED</p>
      </div>
      <h3>[Epoch-3] MerkleTree Notarization (Reissuance)</h3> <br />
      <h4>SUMMARY</h4>
      <p className={Styles.summary}>
        A reporting issue has been spotted during the pre-staging control
        activities before Epoch-3 (December 2021) distribution. This issue has
        been fixed and a new report and Merkle-tree generated to be notarized
        prior to istribution. Withdrawals will be disabled during the transition
        between Epochs. Links
      </p>
    </div>
  );

  return (
    <div>
      <div className={Styles.jumbo}>
        <img className={Styles.hercules} src={Hercules} alt="hercules logo" />
        <h4>Hercules Governance token</h4>
        <h1 className={Styles.title}>HCF</h1>
        <h5>
          Contributed and be rewarded for building <br /> a better organization
          and products
        </h5>
        <button>
          <img src={HCFLogo} alt="logo" />
          <p>
            BUY HCF NOW <br /> <span>current price $0.78</span>
          </p>
          <img src={RightIcon} alt="right" />
        </button>
        <h5 className={Styles.yellow}>Stake hcf</h5> <br />
        <h5>add hcf to meta mask or other waller</h5>
        <div className={Styles.wallet}>
          <img src={MetaMask} alt="Metamask wallet" />
          <img src={Rainbow} alt="Rainbow wallet" />
          <img src={Pillar} alt="Pillar wallet" />
          <a href="#">View all</a>
        </div>
      </div>

      <div className={Styles.container}>
        <div className={Styles.features}>
          {featuresText.map((text, k) => (
            <FeaturesCard title={text} key={k} />
          ))}
        </div>
        <Section title="HCF KEY STATS" titleIcon={ChartIcon}>
          <div className={Styles.statsContainer}>
            <h2>
              <span>
                The hcf token is a proof-of-stake token used to secure hercules
                Network's mainnet, called <br /> Basechain. hcf holders can
                stake their tokens to help secure Basechain and earn rewards{" "}
                <br /> in the process.
              </span>
            </h2>
            <div className={Styles.stats}>
              <HcfCard
                data={dummyData.stats[1]}
                backgroundColor="#e3f9e6"
                icon={HCF}
              />
              <HcfCard
                data={dummyData.stats[2]}
                backgroundColor="#F9E3F4"
                icon={HCFONLYLOGO}
              />
              <HcfCard
                data={dummyData.stats[3]}
                backgroundColor="#E2E2E2"
                icon={LockPng}
              />
              <HcfCard
                data={dummyData.stats[4]}
                backgroundColor="#E2E2E2"
                icon={Group}
              />
            </div>
          </div>
        </Section>
        <Section title="Herconomics" titleIcon={ChartIcon} subTitle={subTitle}>
          <img
            className={Styles.imgSection}
            src={herculesGif}
            alt="herconomics"
          />
          <img className={Styles.imgSection} src={BuyHCF} alt="herconomics" />

          <div className={Styles.statsContainer}>
            <h2 className={Styles.governanceTitle}>
              <img src={Shield} alt="Shield" />
              <p>
                Governance <br />
                <span>Participate on the last Governance decisions</span>
              </p>
            </h2>

            <div>
              <GovernanceCard />
              <GovernanceCard />
            </div>
          </div>
        </Section>
      </div>
    </div>
  );
};

export default HCFToken;
