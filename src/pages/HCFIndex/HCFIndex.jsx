import React from "react";
import BuyItemCard from "../../components/Cards/BuyItemCard";
import Header from "../../components/Header/Header";
import dummyData from "../../utils/dummyData";
import VaultItem from "../../components/VaultItem/VaultItem";
import Charts from "../../components/Charts/Index";

// images
import cam from "./../../images/herc-cam.png";
import hercImg from "./../../images/herc-logo-circ.png";
import hercBnb from "./../../images/herc-bnb.png";
import hercDash from "./../../images/herc-dash.png";
import hercGlobe from "./../../images/herc-globe.png";
import hercN from "./../../images/herc-n.png";
import hercX from "./../../images/herc-x.png";
import hercW from "./../../images/herc-w.png";
import hercA from "./../../images/herc-a.png";
import hercB from "./../../images/herc-b.png";
import hercT from "./../../images/herc-t.png";
import hercM from "./../../images/herc-m.png";
import hercTri from "./../../images/herc-tri.png";
import HerculesImg from "../../images/herc-logo.png";

import "./HCFIndex.css";
import styles from "../Vault/Vault.module.css";
import Footer from "../../components/Footer/Footer";

const HCFIndex = () => {
  const itemData = {
    name: "HCF",
    currentPrice: "3.62",
    percentageIncrease: "2.06",
    text: "Metaverse NFT Index",
    img: HerculesImg,
  };

  return (
    <div className="hcf-index">
      <div className="container">
        <div className="buy-and-sell">
          <BuyItemCard itemData={itemData} />

          <div className="item price-details">
            <section className="nav">
              <h2>
                $2.98 <br /> NAV
              </h2>
            </section>
            <section className="premium">
              <h2>
                2.74% <br /> PREMIUM
              </h2>
            </section>
            <section className="tot-apy">
              <h2>
                0.00% <br /> TOT APY
              </h2>
            </section>
            <section className="market-cap">
              <h2>
                $4,568,202.35 <br /> MARKET CAP
              </h2>
            </section>
          </div>

          <div className="item action">
            <button>
              BUY AND SELL <br /> <span>INSTANT SWAP</span>
            </button>
            <button>
              BAKE YOUR HCF <br /> <span>Wait and save 98% gas</span>
            </button>
          </div>
        </div>

        <Charts />

        <div className={styles.sec1} style={{ margin: 0, width: "100%" }}>
          <div className={styles.heading}>
            <h2>
              <img src={cam} alt="" />
              Allocation Breakdown
            </h2>
            <p>Yield Bearing & Meta-Governance Enabled</p>
          </div>
          <div className={styles.container}>
            <div className={styles.label}>
              <p>NAME</p>
              <p className={styles.asset}>ASSET</p>
              <p className={styles.change}>24 CHANGE</p>
              <p>CURRENT PRICE</p>
              <p>BUY</p>
            </div>
            <VaultItem
              data={dummyData.vaultItem[1]}
              icon1={hercBnb}
              icon2={hercDash}
              icon3={hercGlobe}
              icon4={hercN}
              icon5={hercX}
              icon6={hercW}
              icon7={hercTri}
              mainIcon={hercImg}
            />
            <VaultItem
              data={dummyData.vaultItem[2]}
              icon1={hercBnb}
              icon2={hercDash}
              icon3={hercGlobe}
              icon4={hercN}
              icon5={hercX}
              icon6={hercW}
              icon7={hercTri}
              mainIcon={hercT}
            />
            <VaultItem
              data={dummyData.vaultItem[3]}
              icon1={hercBnb}
              icon2={hercDash}
              icon3={hercGlobe}
              icon4={hercN}
              icon5={hercX}
              icon6={hercW}
              icon7={hercTri}
              mainIcon={hercW}
            />
            <VaultItem
              data={dummyData.vaultItem[4]}
              icon1={hercBnb}
              icon2={hercDash}
              icon3={hercGlobe}
              icon4={hercN}
              icon5={hercX}
              icon6={hercW}
              icon7={hercTri}
              mainIcon={hercB}
              color="#FF5A5A"
            />
            <VaultItem
              data={dummyData.vaultItem[5]}
              icon1={hercBnb}
              icon2={hercDash}
              icon3={hercGlobe}
              icon4={hercN}
              icon5={hercX}
              icon6={hercW}
              icon7={hercTri}
              mainIcon={hercM}
            />
            <VaultItem
              data={dummyData.vaultItem[6]}
              icon1={hercBnb}
              icon2={hercDash}
              icon3={hercGlobe}
              icon4={hercN}
              icon5={hercX}
              icon6={hercW}
              icon7={hercTri}
              mainIcon={hercA}
            />
          </div>
        </div>

        <div className="text-body">
          <h2>Summary:</h2>
          <p>
            HCF provides exposure to the most in demand sectors defining the
            Metaverse. It seeks to track an index of blockchain gaming,
            infrastructure and entertainment projects. Web3 backers looking to
            gain exposure to high liquidity Metaverse assets should consider
            this token.
          </p>
          <br />
          <p>HCF allows investors to: </p>
          <ol>
            <li>
              Invest in the paradigm-shifting Metaverse ecosystem powered by
              blockchain
            </li>
            <li>
              Diversify their portfolios while delegating research and due
              diligence to the PieDAO community
            </li>
            <li>Mitigate gas costs from managing their positions</li>
          </ol>
          <h2>Objective:</h2>
          <p>
            The metaverse is a virtual space created by the convergence of
            physical reality, augmented reality and the internet. While no one
            knows for sure what the metaverse will look like, its basic
            characteristics are established – it spans physical and virtual
            worlds, is centered around a functioning economy, and allows users
            to experience its different places with relative ease: The virtual
            world of the metaverse could become its own trillion-dollar
            industry, being built using blockchains and decentralized
            applications.{" "}
          </p>{" "}
          <br />
          <p>
            Blockchain gaming and metaverses, in particular, are an ambitious
            category of the web3 ecosystem in general. The combination of
            virtual reality and web3, where ownership inside the virtual world
            is retained by users is a match made in heaven.{" "}
          </p>
          <h2>Methodology:</h2>
          <p>
            The PieDAO community has established a set of criteria to determine
            a project's eligibility for inclusion. The criteria seek to achieve
            exposure to projects whose tokens have high liquidity, security, and
            provide real value to holders.{" "}
          </p>{" "}
          <br />
          <p style={{ color: "blue" }}>
            The full PLAY Metaverse Index prospectus is here, covering:{" "}
          </p>
          <ol>
            <li>Eligibility criteria for inclusion in PLAY</li>
            <li>Methodology for asset allocation</li>
            <li>Rules and procedure for rebalancing</li>
          </ol>
          <br />
          <p>
            The allocation methodology considered for initial reference is the
            Correlated Risk-Adjusted Market Cap, same as per other PieDAO
            products, improved by weighting in for each underlying asset the 30
            & 15 Day Average Circulating Market Cap, and its 15 Day Average
            Liquidity on L1 DEXes.
          </p>{" "}
          <br />
          <p>
            The resulting % allocation is further adjusted through the following
            criteria:{" "}
          </p>
          <ol>
            <li>Minimum allocation of 2% for each underlying asset</li>
            <li>
              Max allocation of 30% for single large cap holding, to limit the
              predominance of a single large cap holding
            </li>
            <li>
              Max cumulative allocation of 60% for the top 3 large cap holdings,
              to eventually prevent the excessive skewness of the Pie toward
              large caps tokens
            </li>
            <li>
              Max allocation per underlying asset is capped to limit the
              slippage occurring when buying the assets on L1 DEXes
            </li>
            <li>
              Weighting in the momentum of the asset Market Cap over the past 30
              days
            </li>
          </ol>
          <h2>Operation: </h2>
          <p>
            PLAY is a PieVault, benefitting from a more secure architecture than
            traditional AMM based ETF products. PieVaults allow flexibility in
            the allocation, enabling holders to benefit from upward price
            movements without ‘selling the winners’ and buying the losers.{" "}
          </p>{" "}
          <br />
          <p>
            Just like existing PieVaults, PLAY will enable meta-governance, with
            DOUGH holders able to vote using the underlying assets in their
            respective protocol decisions.{" "}
          </p>{" "}
          <br />
          <p>
            Active strategies are also possible and will be activated over time
            as they become available, with the PieVault bouncing between
            strategies as yields change to maintain the highest possible return
            for holders. PLAY holders passively benefit from this intrinsic
            productivity, without having to perform any action themselves.{" "}
          </p>{" "}
          <br />
          <p>
            The rebalancing criteria adopted for PLAY is ruled-based, taking in
            consideration both a subset of objective conditions playing as
            triggers for rebalancing, and a broader community consensus, which
            would decide on the rebalancing urgency based on some reference
            guidelines defined in the prospectus, as a conscious effort to
            optimize the overall rebalancing cost by preventing unnecessary
            rebalancings.{" "}
          </p>{" "}
          <br />
        </div>
      </div>
    </div>
  );
};

export default HCFIndex;
