import Styles from "./Campaign.module.css";
import Header from "../../components/Header/Header";
import Section from "../../components/Section/Section";
import HcfCard from "../../components/HcfCard/HcfCard";
import Footer from "../../components/Footer/Footer";
import dummyData from "../../utils/dummyData";

import metamask from "./../../images/metamask.png";
import rainbow from "./../../images/rainbow.png";
import pillar from "./../../images/pillar.png";
import UMALogo from "../../images/UMALogo.png";
import HCF from "./../../images/HCF.png";
import HCFONLYLOGO from "./../../images/HF-LOGO-ICON-ONLY.png";
import LockPng from "./../../images/Lock.png";
import Group from "./../../images/Group.png";
import CampaignImage from "./../../images/campaign.png"

function Campaign() {
  const participateList = [
    {
      title: "Stake HCF by May 31st 2021 and claim Hercules-kpi",
      backgroundColor: "#E5F3FF",
    },
    {
      title: "Invite as many people as possible to stake by May  30th 2022",
      backgroundColor: "#E5FFEB",
    },
    {
      title: "Use your Hercules-kpi to redeem the HCF payout",
      backgroundColor: "#FFF6E5",
    },
  ];

  const SummaryList = [
    {
      title: "Total distribited KPI Options",
      amount: "15,000,000.00",
    },
    {
      title: "Your Claimable KPI Options",
      amount: "0",
    },
    {
      title: "Your Estimated Payout",
      amount: "0",
    },
  ];

  const Guide = [
    {
      title: "Staking user guide",
      backgroundColor: "#63B4FF",
    },
    {
      title: "Learn about UMA KPI options",
      backgroundColor: "#7ADA8F",
    },
    {
      title: "Join our Discord",
      backgroundColor: "#E2A6FF",
    },
  ];

  const ParticipateCard = ({ title, number, backgroundColor }) => (
    <div style={{ backgroundColor }} className={Styles.card}>
      {number && <h1>{number}</h1>}
      <p>{title}</p>
    </div>
  );

  const SummaryCard = ({ title, amount }) => (
    <div className={Styles.SummaryCard}>
      <h2>{title}</h2>
      <p>
        {amount} <img src={HCF} alt="hcf logo" /> HCF{" "}
      </p>
    </div>
  );

  return (
    <div>
      <Section>
        <div className={Styles.jumbo}>
          <div>
            <img src={UMALogo} alt="logo" />
            <h1>
              5 million <br /> hercules <br /> price
            </h1>
            <p>
              If 15M HCF are staked by <br /> May 30th, 2022
            </p>
          </div>
          <img className={Styles.campaignImage} src={CampaignImage} alt="campaign" />
        </div>
      </Section>
      <Section childShadow>
        <div className={Styles.participate}>
          <h1>How can i participate?</h1>
          <p>
            Everyone that has staked Dough during October 2021 is eligible! If
            you did check your address here The options have been distributed
            already, check your balance in the summary below. <br /> <br />
            Now its time to pump those numbers up!
          </p>
          <div className={Styles.cardSection}>
            {participateList.map((item, k) => (
              <ParticipateCard
                key={k}
                number={k + 1}
                title={item.title}
                backgroundColor={item.backgroundColor}
              />
            ))}
          </div>
        </div>
      </Section>

      <Section title="Where are we right now" childShadow>
        <div className={Styles.whereAreWe}>
          <HcfCard
            data={dummyData.stats[1]}
            backgroundColor="#e3f9e6"
            icon={HCF}
          />
          <HcfCard
            data={dummyData.stats[2]}
            backgroundColor="#F9E3F4"
            icon={HCFONLYLOGO}
          />
          <HcfCard
            data={dummyData.stats[3]}
            backgroundColor="#E2E2E2"
            icon={LockPng}
          />
          <HcfCard
            data={dummyData.stats[4]}
            backgroundColor="#E2E2E2"
            icon={Group}
          />
        </div>
      </Section>

      <Section title="Summary" childShadow>
        {SummaryList.map(({ title, amount }, k) => (
          <SummaryCard title={title} key={k} amount={amount} />
        ))}
        <div className={Styles.connectWallet}>
          <h2>add hcf to meta mask or other waller</h2>
          <div>
            <img src={metamask} alt="Metamask" />
            <img src={rainbow} alt="Rainbow" />
            <img src={pillar} alt="Pillar" />
            <a href="#">View all</a>
          </div>
        </div>
      </Section>

      <Section>
        <div className={Styles.cardSection}>
          {Guide.map((item, k) => (
            <ParticipateCard
              title={item.title}
              key={k}
              backgroundColor={item.backgroundColor}
            />
          ))}
        </div>

        <div className={Styles.share}>
          <h2>
            If we managed to hit the milestone of over 20% HCF staked in under a
            week by the efforts of a handful of individuals, can you imagine
            where we can get by coordinated efforts for several-thousand people
            community? Sky is the limit.
          </h2>
          <button>Share</button>
        </div>
      </Section>
    </div>
  );
}

export default Campaign;
