import React, { useEffect, useState } from "react";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

import Section from "../../components/Section/Section";
import SectionWithBg from "../../components/SectionWithBg/SectionWithBg";

import FarmIcon from "../../images/farm-icon.png";
import Dislike from "../../images/Dislike.png";
import SwapVaultItem from "../../components/VaultItem/SwapVaultItem";

import HCFIcon from "../../images/herc-logo-circ.png";
import Ether from "../../images/ethereum.png";

import Styles from "./Farm.module.css";
const Farm = () => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    setIsMobile(window.innerWidth <= 480);
  }, []);
  return (
    <div>
      <div className={Styles.reverse}>
        <div className={Styles.spacing}>
          <Section title="Farm Pools" titleIcon={FarmIcon}>
            {[1, 2, 3, 4, 5, 6].map((item) => (
              <SwapVaultItem
                key={item}
                images={[HCFIcon, Ether]}
                swapTitle="HCF/ETH"
                details="20% Liquid - 80% Escrowed"
                left="Sushi"
              />
            ))}
            <br />
            <p align="right">
              <strong>
                Looking for APR? Go to{" "}
                <a href="https://vat.tools">vfat.tools!</a>
              </strong>
            </p>
          </Section>
        </div>
        <SectionWithBg>
          {isMobile ? (
            <div>
              <div>
                <h2>Farm Positions</h2>
                <p>Connect web3 to see your positions</p>
              </div>
              <img align="right" src={Dislike} alt="Dislike" />
            </div>
          ) : (
            <>
              <div>
                <h2>Farm Positions</h2>
                <p>Connect web3 to see your positions</p>
              </div>
              <img src={Dislike} alt="Dislike" />
            </>
          )}
        </SectionWithBg>
      </div>
      <br />
      <br />
      <br />
    </div>
  );
};

export default Farm;
