import Header from "../../components/Header/Header";
import Section from "../../components/Section/Section";
import Styles from "./Portfolio.module.css";
import dummyData from "../../utils/dummyData";
import HcfCard from "../../components/HcfCard/HcfCard";
import Footer from "../../components/Footer/Footer"

import BuyHCF from "../../images/buyHCF.png"
import IceCream from "../../images/Ice-cream#1.png";
import Wallet from "../../images/Wallet.png";
import Like from "../../images/Dislike.png";
import ShieldCheck from "../../images/Shield-check.png";
import Export from "../../images/Export.png";
import File from "../../images/File.png";
import Icon from "../../images/Sketch.png";
import HcfCardPortfolio from "../../components/HcfCard/HcfCardPortfolio";

const Portfolio = ({state}) => {
  const Card = ({ title, icon, children, rightIcon }) => (
    <div className={Styles.portfolioCard}>
      {title && (
        <h2 className={Styles.title}>
          <img src={icon} alt="Ice cream" />
          {title}
          {rightIcon && (
            <img src={rightIcon} alt="export" className={Styles.right} />
          )}
        </h2>
      )}
      <div>{children}</div>
    </div>
  );

  const value = [
    state.hcfToken_supply?(state.totalStaked/state.hcfToken_supply).toFixed(5)*100:0,
    state.hcfToken_supply?(state.totalStaked/state.hcfToken_supply).toFixed(5)*100:0,
    state.accountWithdrawableRewards,
    state.accountAverage_month,
    state.voting_power
  ]

  return (
    <div>
      <Section>
        <div className={Styles.container}>
          <Card title="Your Holdings" icon={IceCream}></Card>
          <Card title="Summary" icon={File}>
            {[1, 2, 3, 4, 5].map((item) => (
              <div className={Styles.hcfCard}>
                <HcfCard
                  key={item}
                  data={dummyData.summary[item]}
                  value={[value[item-1]]}
                  icon={Icon}
                />
              </div>
            ))}
          </Card>
          <Card title="Wallet Allocation / n/a" icon={Wallet}></Card>
          <Card title="Farm positions" icon={Like}>
            {[1, 2, 3, 4, 5].map((item) => (
              <div className={Styles.hcfCard}>
                <HcfCardPortfolio
                  governanceReward={item === 5 && true}
                  key={item}
                  data={dummyData.summary[item]}
                  icon={Icon}
                />
              </div>
            ))}
          </Card>
          <Card />
          <Card title="Governance" icon={ShieldCheck} rightIcon={Export}>
            <HcfCardPortfolio>
              <div className={Styles.governance}>
                <h4>[Epoch-3] Markletree Notarization</h4>
                <div>
                  <button>Closed</button>
                  <h5>Ends on: Feb 29 2020</h5>
                </div>
              </div>
            </HcfCardPortfolio>
            <br />
            <HcfCardPortfolio>
              <div className={Styles.governance}>
                <h4>[Epoch-3] Markletree Notarization</h4>
                <div>
                  <button>Closed</button>
                  <h5>Ends on: Feb 29 2020</h5>
                </div>
              </div>
            </HcfCardPortfolio>
          </Card>
        </div>

        <div className={Styles.buyHCF}>
              <img src={BuyHCF} alt="buy hcf" />
        </div>
      </Section>

    </div>
  );
};

export default Portfolio;
