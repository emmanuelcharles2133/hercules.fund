import React from "react";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";

// images
import stack from "./../../images/stack.svg";
import cone from "./../../images/cone.png";
import bell from "./../../images/bell.png";
import logo from "./../../images/hcf-logo-only-blue.png";
import timer from "./../../images/timer.png";
import hercLogo from "./../../images/herc-logo.png";
import freezeIllustration from "./../../images/freeze.png";

import styles from "./Freeze.module.css";

const Freeze = () => {
  const freeze = {
    1: [
      {
        Name: "PLAY",
        Percentage: "24.8%",
      },
    ],
    2: [
      {
        Name: "HCF",
        Percentage: "55%",
      },
    ],
    3: [
      {
        Name: "HCF",
        Percentage: "82.3%",
      },
    ],
    4: [
      {
        Name: "PLAY",
        Percentage: "31%",
      },
    ],
    5: [
      {
        Name: "HCF",
        Percentage: "95%",
      },
    ],
    6: [
      {
        Name: "HCF",
        Percentage: "69%",
      },
    ],
  };

  return (
    <div>
      <div className={styles.sec1}>
        <div className={styles.heading}>
          <h2>
            <img src={cone} alt="" />
            COOK TOGETHER, SAVE 97% GAS.
          </h2>
          <p>
            GET HOLD OF YOUR TOKEN ALMOST GAS-FREE BY SHARING THE COST. <br />
            TURN YOUR ETH INTO COOL TOKEN IN THREE STEPS.
          </p>
        </div>
        <div className={styles.info}>
          <div>
            <img src={bell} alt="" />
            <div>
              <h3>FREEZE ETH</h3>
              <p>WHEN AT LEAST 10 ETH IS DEPOSITED, THE FREEZE CAN BEGIN</p>
            </div>
          </div>
          <div>
            <img src={timer} alt="" />
            <div>
              <h3>WAIT</h3>
              <p>
                FREEZE WILL BE READY WHEN GAS PRICE IS BELOW 100, SAVING
                EVERYONE MONEY
              </p>
            </div>
          </div>
          <div>
            <img src={logo} alt="" />
            <div>
              <h3>WITHDRAW YOUR TOKENS</h3>
              <p>ONCE THE TOKEN IS READY, YOU CAN WITHDRAW IT TO YOUR WALLET</p>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.sec2}>
        <div className={styles.heading}>
          <h2>
            <img src={stack} alt="" />
            SELECT YOUR POOL
          </h2>
        </div>
        <div className={styles.tab2}>
          <div class="gradient-border">
            <div className="item grad-text">
              <div className="top">
                {freeze[1][0].Name} <br />
                <div className={styles.reach}>
                  <p>MAXIMUM REACHED</p>
                  <div className={styles.progress}>
                    <div
                      className={styles.value}
                      style={{ width: `${freeze[1][0].Percentage}` }}
                    >
                      {freeze[1][0].Percentage}
                    </div>
                  </div>
                </div>
                <div class="hercImg">
                  <img src={hercLogo} alt="" />
                </div>
              </div>
            </div>
            <div className="bottom">
              <span className={styles.deposit}>DEPOSIT / WITHDRAW</span>
            </div>
          </div>
          <div class="gradient-border">
            <div className="item grad-text">
              <div className="top">
                {freeze[2][0].Name} <br />
                <div className={styles.reach}>
                  <p>MAXIMUM REACHED</p>
                  <div className={styles.progress}>
                    <div
                      className={styles.value}
                      style={{ width: `${freeze[2][0].Percentage}` }}
                    >
                      {freeze[2][0].Percentage}
                    </div>
                  </div>
                </div>
                <div class="hercImg">
                  <img src={hercLogo} alt="" />
                </div>
              </div>
            </div>
            <div className="bottom">
              <span className={styles.deposit}>DEPOSIT / WITHDRAW</span>
            </div>
          </div>
          <div class="gradient-border">
            <div className="item grad-text">
              <div className="top">
                {freeze[3][0].Name} <br />
                <div className={styles.reach}>
                  <p>MAXIMUM REACHED</p>
                  <div className={styles.progress}>
                    <div
                      className={styles.value}
                      style={{ width: `${freeze[3][0].Percentage}` }}
                    >
                      {freeze[3][0].Percentage}
                    </div>
                  </div>
                </div>
                <div class="hercImg">
                  <img src={hercLogo} alt="" />
                </div>
              </div>
            </div>
            <div className="bottom">
              <span className={styles.deposit}>DEPOSIT / WITHDRAW</span>
            </div>
          </div>
          <div class="gradient-border">
            <div className="item grad-text">
              <div className="top">
                {freeze[4][0].Name} <br />
                <div className={styles.reach}>
                  <p>MAXIMUM REACHED</p>
                  <div className={styles.progress}>
                    <div
                      className={styles.value}
                      style={{ width: `${freeze[4][0].Percentage}` }}
                    >
                      {freeze[4][0].Percentage}
                    </div>
                  </div>
                </div>
                <div class="hercImg">
                  <img src={hercLogo} alt="" />
                </div>
              </div>
            </div>
            <div className="bottom">
              <span className={styles.deposit}>DEPOSIT / WITHDRAW</span>
            </div>
          </div>
          <div class="gradient-border">
            <div className="item grad-text">
              <div className="top">
                {freeze[5][0].Name} <br />
                <div className={styles.reach}>
                  <p>MAXIMUM REACHED</p>
                  <div className={styles.progress}>
                    <div
                      className={styles.value}
                      style={{ width: `${freeze[5][0].Percentage}` }}
                    >
                      {freeze[5][0].Percentage}
                    </div>
                  </div>
                </div>
                <div class="hercImg">
                  <img src={hercLogo} alt="" />
                </div>
              </div>
            </div>
            <div className="bottom">
              <span className={styles.deposit}>DEPOSIT / WITHDRAW</span>
            </div>
          </div>
          <div class="gradient-border">
            <div className="item grad-text">
              <div className="top">
                {freeze[6][0].Name} <br />
                <div className={styles.reach}>
                  <p>MAXIMUM REACHED</p>
                  <div className={styles.progress}>
                    <div
                      className={styles.value}
                      style={{ width: `${freeze[6][0].Percentage}` }}
                    >
                      {freeze[6][0].Percentage}
                    </div>
                  </div>
                </div>
                <div class="hercImg">
                  <img src={hercLogo} alt="" />
                </div>
              </div>
            </div>
            <div className="bottom">
              <span className={styles.deposit}>DEPOSIT / WITHDRAW</span>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.sec3}>
        <h3>CAN'T WAIT?</h3>
        <p>
          YOU CAN ALWAYS BUY YOUR TOKEN NSTANTLY FROM THE EXCHANGE <br /> PAGE.
        </p>
        <button>SELECT YOUR TOKEN</button>
      </div>
      <div className={styles.sec4}>
        <div className={styles.heading}>
          <h2>HOW DOES THE FREEZE WORK?</h2>
        </div>
        <img src={freezeIllustration} alt="" />
      </div>
      <div className={styles.sec5}>
        <div className={styles.heading}>
          <h2>DO YOU HAVE QUESTIONS?</h2>
        </div>
        <div className={styles.questions}>
          <details>
            <summary>HOW DOES OVEN SAVE ME 97% GAS?</summary>
            <p>
              With the community Oven users bake their pies together. Normally
              baking can require as much as 73 separate transactions for each
              user, costing a large amount in gas. Oven bakes everyone’s pies
              together, reducing the total number of transactions needed and
              sharing the gas costs.
            </p>
          </details>
          <details>
            <summary>WHEN WILL OVEN BE TRIGGERED?</summary>
            <p>
              With the community Oven users bake their pies together. Normally
              baking can require as much as 73 separate transactions for each
              user, costing a large amount in gas. Oven bakes everyone’s pies
              together, reducing the total number of transactions needed and
              sharing the gas costs.
            </p>
          </details>
          <details>
            <summary>WHY DOES IT SAY 100% BUT IS STILL NOT FREEZING?</summary>
            <p>
              With the community Oven users bake their pies together. Normally
              baking can require as much as 73 separate transactions for each
              user, costing a large amount in gas. Oven bakes everyone’s pies
              together, reducing the total number of transactions needed and
              sharing the gas costs.
            </p>
          </details>
          <details>
            <summary>HOW LONG DOES IT USUALLY TAKE TO BAKE A HCF?</summary>
            <p>
              With the community Oven users bake their pies together. Normally
              baking can require as much as 73 separate transactions for each
              user, costing a large amount in gas. Oven bakes everyone’s pies
              together, reducing the total number of transactions needed and
              sharing the gas costs.
            </p>
          </details>
          <details>
            <summary>HOW DOES OVEN SAVE ME 97% GAS?</summary>
            <p>
              With the community Oven users bake their pies together. Normally
              baking can require as much as 73 separate transactions for each
              user, costing a large amount in gas. Oven bakes everyone’s pies
              together, reducing the total number of transactions needed and
              sharing the gas costs.
            </p>
          </details>
          <details>
            <summary>HOW DOES OVEN SAVE ME 97% GAS?</summary>
            <p>
              With the community Oven users bake their pies together. Normally
              baking can require as much as 73 separate transactions for each
              user, costing a large amount in gas. Oven bakes everyone’s pies
              together, reducing the total number of transactions needed and
              sharing the gas costs.
            </p>
          </details>
          <details>
            <summary>HOW DOES OVEN SAVE ME 97% GAS?</summary>
            <p>
              With the community Oven users bake their pies together. Normally
              baking can require as much as 73 separate transactions for each
              user, costing a large amount in gas. Oven bakes everyone’s pies
              together, reducing the total number of transactions needed and
              sharing the gas costs.
            </p>
          </details>
        </div>
      </div>
    </div>
  );
};

export default Freeze;
