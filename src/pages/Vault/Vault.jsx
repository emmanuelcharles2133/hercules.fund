import React, { useState, useEffect } from "react";
import dummyData from "../../utils/dummyData";
import VaultItem from "../../components/VaultItem/VaultItem";

// images
import cam from "./../../images/herc-cam.png";
import hercImg from "./../../images/herc-logo-circ.png";
import hercBnb from "./../../images/herc-bnb.png";
import hercDash from "./../../images/herc-dash.png";
import hercGlobe from "./../../images/herc-globe.png";
import hercN from "./../../images/herc-n.png";
import hercW from "./../../images/herc-w.png";
import hercA from "./../../images/herc-a.png";
import hercB from "./../../images/herc-b.png";
import hercT from "./../../images/herc-t.png";
import hercM from "./../../images/herc-m.png";
import hercTri from "./../../images/herc-tri.png";

import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import styles from "./Vault.module.css";
import Table from "../../components/Table/Table";

const Vault = () => {
  const titles = ["Name", "Assets", "24 Change", "Current Price", "Buy"]
  const [isMobile, setIsMobile] = useState(false)

  useEffect(() => {
    setIsMobile(window.innerWidth <= 480)
  }, [])

  return (
    <div>
      <div className={styles.sec1}>
        <div className={styles.heading}>
          <h2>
            <img src={cam} alt="" />
            HCF VAULTS{" "}
          </h2>
          <p>Yield Bearing & Meta-Governance Enabled</p>
        </div>
        <div className={styles.container}>
          <Table titles={!isMobile && titles}>
          <VaultItem
            data={dummyData.vaultItem[1]}
            icons={[hercN, hercW, hercTri, hercImg, hercBnb, hercDash, hercGlobe]}
            mainIcon={hercImg}
          />
          <VaultItem
            data={dummyData.vaultItem[2]}
            icons={[hercBnb, hercDash, hercGlobe, hercN, hercW, hercTri, hercImg]}
            mainIcon={hercT}
          />
          <VaultItem
            data={dummyData.vaultItem[3]}
            icons={[hercN, hercW, hercTri, hercImg, hercBnb, hercDash, hercGlobe]}
            mainIcon={hercW}
          />
          <VaultItem
            data={dummyData.vaultItem[4]}
            icons={[hercBnb, hercDash, hercGlobe, hercN, hercW, hercTri, hercImg]}
            mainIcon={hercB}
            color="#FF5A5A"
          />
          <VaultItem
            data={dummyData.vaultItem[5]}
            icons={[hercN, hercW, hercTri, hercImg, hercBnb, hercDash, hercGlobe]}
            mainIcon={hercM}
          />
          <VaultItem
            data={dummyData.vaultItem[6]}
            icons={[hercBnb, hercDash, hercGlobe, hercN, hercW, hercTri, hercImg]}
            mainIcon={hercA}
          />
          </Table>
        </div>
      </div>

      <div className={styles.sec1}>
        <div className={styles.heading}>
          <h2>
            <img src={cam} alt="" />
            EXPLORE HCF
          </h2>
          <p>Yield Bearing & Meta-Governance Enabled</p>
        </div>
        <div className={styles.container}>
        <Table titles={!isMobile && titles}>
          <VaultItem
            data={dummyData.vaultItem[1]}
            icons={[hercN, hercW, hercTri, hercImg, hercBnb, hercDash, hercGlobe]}
            mainIcon={hercImg}
          />
          <VaultItem
            data={dummyData.vaultItem[2]}
            icons={[hercBnb, hercDash, hercGlobe, hercN, hercW, hercTri, hercImg]}
            mainIcon={hercT}
          />
          <VaultItem
            data={dummyData.vaultItem[3]}
            icons={[hercN, hercW, hercTri, hercImg, hercBnb, hercDash, hercGlobe]}
            mainIcon={hercW}
          />
          <VaultItem
            data={dummyData.vaultItem[4]}
            icons={[hercBnb, hercDash, hercGlobe, hercN, hercW, hercTri, hercImg]}
            mainIcon={hercB}
            color="#FF5A5A"
          />
          <VaultItem
            data={dummyData.vaultItem[5]}
            icons={[hercN, hercW, hercTri, hercImg, hercBnb, hercDash, hercGlobe]}
            mainIcon={hercM}
          />
          <VaultItem
            data={dummyData.vaultItem[6]}
            icons={[hercBnb, hercDash, hercGlobe, hercN, hercW, hercTri, hercImg]}
            mainIcon={hercA}
          />
          </Table>
        </div>
      </div>
    </div>
  );
};

export default Vault;
