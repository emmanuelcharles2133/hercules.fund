import React, { useState } from "react";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import HcfCard from "../../components/HcfCard/HcfCard";
import dummyData from "../../utils/dummyData";

// images
import info from "./../../images/info.png";
import galaxy from "./../../images/galaxy.png";
import coins from "./../../images/coins.png";
import glove from "./../../images/glove.png";
import sunbg from "./../../images/sunbg.png";
import HCFONLYLOGO from "./../../images/HF-LOGO-ICON-ONLY.png";
import chartLine from "./../../images/Chart-line.png";
import HCF from "./../../images/HCF.png";
import LockPng from "./../../images/Lock.png";
import Group from "./../../images/Group.png";
import sketch from "./../../images/Sketch.png";
import thunder from "./../../images/Thunder-move.png";

// gif
import hercules from "./../../images/hercules.gif";

// graphs
import ReturnsGraph from "../../components/Charts/returnGraph";
import RewardGraph from "../../components/Charts/rewardGraph";
import TreasuryGraph from "../../components/Charts/treasuryGraph";

import styles from "./Staking.module.css";
import BuyHCF from "./../../components/BuyHCF/BuyHCF";

const StakingSimulator = () => {
  const [treasury, setTreasury] = useState("12%");
  const [reward, setReward] = useState("35%");
  const [months, setMonths] = useState("36");
  const [graph, setGraph] = useState("returnsGraph");

  const re = /^[0-9\b]+$/;

  const handleMonths = (e) => {
    if (e.target.value === "" || re.test(e.target.value)) {
      setMonths(e.target.value);
    }
  };

  const handleTreasury = (e) => {
    if (e.target.value === "" || re.test(e.target.value)) {
      setTreasury(e.target.value);
    }
  };

  const addTreasuryPercentage = (e) => {
    if (
      e.target.value[e.target.value.length - 1] !== "%" &&
      e.target.value !== ""
    ) {
      setTreasury(e.target.value + "%");
    }
  };

  const addRewardPercentage = (e) => {
    if (
      e.target.value[e.target.value.length - 1] !== "%" &&
      e.target.value !== ""
    ) {
      setReward(e.target.value + "%");
    }
  };

  const handleReward = (e) => {
    if (e.target.value === "" || re.test(e.target.value)) {
      setReward(e.target.value);
    }
  };

  return (
    <div>
      <div className={styles.sec1}>
        <div className={styles.left}>
          <h1>
            GET PAID <br />
            FOR GOVERNING <br />
            HCF
          </h1>
          <p>
            A COMPLETE REDESIGN OF THE GOVERNANCE SYSTEM WITH TOKEN HOLDERS IN
            MIND: VOTE ON KEY DAO MATTERS AND GET COMPENSATED FOR YOUR WORK
            EVERY MONTH.
          </p>
        </div>

        <div className={styles.right}>
          <img src={coins} alt="" />
        </div>
      </div>
      <div className={styles.sec2}>
        <BuyHCF />
      </div>
      <div className={styles.sec3}>
        <div className={styles.top}>
          <img src={galaxy} alt="" />
          <img src={galaxy} alt="" />
        </div>

        <div className={styles.bottom}>
          <h3>WE STAND BY OUR PRODUCTS</h3>
          <section className={styles.info}>
            <span>
              OUR PRODUCTS DO WHAT THEY SAY ON THE TIN: DIVERSIFY YOUR PORTFOLIO
              AND MAKE YOU MONEY. THAT IS WHY WE PROPOSE TO ACTIVELY MANAGE OUR
              OWN
            </span>
            <span>
              TREASURY, GENERATING MORE REVENUE FROM LIQUIDITY POOLS ACROSS
              BALANCER, UNISWAP, CURVE, AND SUSHISWAP.
            </span>
          </section>
        </div>
      </div>
      <div className={styles.sec4}>
        <img src={glove} alt="" />
      </div>
      <div className={styles.sec5}>
        <h2>
          GOVERNANCE <br /> MINING
        </h2>
        <p>
          WE BELIEVE THAT DAO PROFITS SHOULD GO TO THOSE WHO PUT IN THE HARD
          WORK OF DRIVING THIS WEALTH CREATION <br /> MACHINE FORWARD: ACTIVE
          GOVERNANCE MEMBERS. <br />
          <br />
          SHOULD PIP-60 PASS THE COMMUNITY VOTE, ONLY STAKED HCF (HERCULES) WILL
          BE USED FOR GOVERNANCE VOTES.
          <br /> THIS MEANS NO FREE-RIDING, NO SWAYING VOTES FOR SHORT-TERM
          GAIN, AND TRUE LONG-TERM ALIGNMENT FOR THE FUTURE OF HERCULES.
        </p>
      </div>
      <div className={styles.sec6}>
        <img src={sunbg} alt="" />
        <div>
          <h2>HERCULES IS THE NEW NORMAL</h2>
          <p>
            ANY HCF HOLDER CAN CHOOSE TO STAKE. THE SELECTED STAKING PERIOD AND
            TOKEN AMOUNT WILL DETERMINE <br /> A USER’S VOTING POWER AND SHARE
            OF DAO PROFITS. THIS COMPLETELY TRANSFORMS DOUGH VALUE <br />{" "}
            ACCRUAL, CREATING A BUY AND HOLD PRESSURE AS A SOURCE OF CASH FLOWS.
            <br />
            <br />
            ACTIVE HCF HOLDERS WILL GET 60% (!) OF DAO PROFITS, WHILE 25% WILL
            BE USED TO COMPOUND THE TREASURY <br /> AND 15% WILL BE ALLOCATED TO
            DEVELOPMENT COSTS.
          </p>
        </div>
      </div>
      <div className={styles.sec7}>
        <div>
          <h1>
            SYNERGISTIC TO THE <br />
            DEFI ECOSYSTEM
          </h1>
          <p>
            HERCULES LIES AT THE HEART OF THE DEFI ECOSYSTEM, AND WE WANT IT TO
            THRIVE. THIS IS WHY WE WILL NOT BE SELLING OUR FARMED TOKENS FOR
            OTHER ASSETS TO DISTRIBUTE TO HCF HOLDERS - WE WILL DISTRIBUTE WHAT
            WE FARM, ALL NICELY PACKED TOGETHER IN OUR SPECIAL REWARDSPIE.
          </p>
        </div>

        <div>
          <h1>
            WHO CALLS THE <br />
            SHOTS?
          </h1>
          <p>
            A SPECIALLY DESIGNATED TREASURY COMMITTEE IS BEING FORMED, WHOSE
            MAIN TASKS WILL BE ENSURING SUFFICIENTLY DIVERSIFIED ALLOCATION OF
            ASSETS, DETERMINING FARMING STRATEGIES TO ESTABLISH CONSISTENT
            RETURNS TO TOKEN HOLDERS, AND BRINGING THE DAO TO
            SELF-SUSTAINABILITY.
          </p>
        </div>
      </div>
      <div className={styles.sec8}>
        <h2>HOW HERCULES WORKS</h2>
        <img src={hercules} alt="" />
      </div>
      <div className={styles.sec9}>
        <h2>
          ANYONE CAN BECOME A <br /> GOVERNANCE MEMBER
        </h2>
        <p>
          GOVERNING HCF IS ABOUT COMMITMENT AND GENUINE PASSION FOR THE MISSION
          - NOT THE SIZE OF YOUR WALLET TO COVER THE GAS FEES FOR VOTES.
        </p>
      </div>
      <div className={styles.sec10}>
        <div className="sec1">
          <h2>
            <img src={chartLine} alt="" className="title-icon" />
            HCF KEY STATS
          </h2>
          <div className="container">
            <h2>
              <span>
                The LOOM token is a proof-of-stake token used to secure Loom
                Network's mainnet, called <br /> Basechain. LOOM holders can
                stake their tokens to help secure Basechain and earn rewards in
                the <br />
                process.
              </span>
            </h2>
            <div className="stats">
              <HcfCard
                data={dummyData.stats[1]}
                backgroundColor="#e3f9e6"
                icon={HCF}
              />
              <HcfCard
                data={dummyData.stats[2]}
                backgroundColor="#F9E3F4"
                icon={HCFONLYLOGO}
              />
              <HcfCard
                data={dummyData.stats[3]}
                backgroundColor="#E2E2E2"
                icon={LockPng}
              />
              <HcfCard
                data={dummyData.stats[4]}
                backgroundColor="#E2E2E2"
                icon={Group}
              />
            </div>
          </div>
        </div>
      </div>
      <div className={styles.sec11}>
        <div className={styles.tab1}>
          <div className={styles.first}>
            <p className={styles.smOp}>
              TREASURY LIQUIDITY DEPLOYED{" "}
              <img src={info} className={styles.infoIco} alt="" />
            </p>
            <p className={styles.fnBg}>$ 12,65 M</p>
            <p className={`${styles.fwBg} ${styles.fmSm}`}>6595.35 ETH</p>
          </div>
          <div className={styles.reward}>
            <p className={styles.smOp}>
              REWARD DISTRIBUTIONS{" "}
              <img src={info} className={styles.infoIco} alt="" />
            </p>
            <div>
              <p className={styles.rewardInfo}>
                Distributed to Hercules holders
                <br />
                Used to compound the treasury
                <br />
                Used to cover costs
                <br />
              </p>
              <span>
                <div className={styles.green}>60%</div>
                <div className={styles.purple}>25%</div>
                <div className={styles.blue}>15%</div>
              </span>
            </div>
          </div>
        </div>
        <div className={styles.tab2}>
          <div className={styles.first}>
            <p className={`${styles.smOp} ${styles.mgTp}`}>
              TREASURY LIQUIDITY DEPLOYED{" "}
              <img src={info} className={styles.infoIco} alt="" />
            </p>
            <span>
              <input type="number" className={styles.inputInvisible} /> &nbsp;{" "}
              <img src={HCF} alt="" /> &nbsp; HCF
            </span>
          </div>
          <div className={styles.second}>
            <p className={styles.smOp}>
              TOTAL STAKING COMMITMENT{" "}
              <img src={info} className={styles.infoIco} alt="" />
            </p>
            <div className={styles.ssecond}>
              <div className={styles.commitment}>
                <div>
                  <div className={styles.progress}></div>{" "}
                  <span>1% 6 MONTHS</span>
                </div>
              </div>
              <div className={styles.commitment}>
                <div>
                  <div className={styles.progress}></div> <span>1% 1 YEAR</span>
                </div>
              </div>
              <div className={styles.commitment}>
                <div>
                  <div className={styles.progress}></div>{" "}
                  <span>1% 2 YEARS</span>
                </div>
              </div>
              <div className={styles.commitment}>
                <div>
                  <div className={styles.progress}></div>{" "}
                  <span>97% 3 YEARS</span>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.third}>
            <div>
              <p className={styles.smOp}>
                EXPECTED TREASURY APR{" "}
                <img src={info} className={styles.infoIco} alt="" />
              </p>
              <div>
                <input
                  className={styles.inputInvisible}
                  maxLength="2"
                  value={treasury}
                  onChange={handleTreasury}
                  onBlur={addTreasuryPercentage}
                />{" "}
                <img src={thunder} alt="" />
              </div>
            </div>
            <div>
              <p className={styles.smOp}>
                REWARDS UNCLAIMED{" "}
                <img src={info} className={styles.infoIco} alt="" />
              </p>
              <div>
                <input
                  className={styles.inputInvisible}
                  maxLength="2"
                  value={reward}
                  onChange={handleReward}
                  onBlur={addRewardPercentage}
                />{" "}
                <img src={sketch} alt="" />
              </div>
            </div>
          </div>
        </div>
        <div className={styles.tab3}>
          <div className={styles.first}>
            <div>
              <p className={`${styles.smOp} ${styles.mgTp}`}>
                YOUR STAKED HCF{" "}
                <img src={info} className={styles.infoIco} alt="" />
              </p>
              <span>
                <input type="number" className={styles.inputInvisible} />{" "}
                <span>
                  {" "}
                  <img src={HCF} alt="" /> &nbsp; HCF{" "}
                </span>{" "}
              </span>
            </div>
            <div>
              <p className={`${styles.smOp} ${styles.mgTp}`}>
                YOUR STAKING COMMITMENT{" "}
                <img src={info} className={styles.infoIco} alt="" />
              </p>
              <span>
                <div>
                  <input
                    type="text"
                    maxLength="2"
                    onChange={handleMonths}
                    value={months}
                    className={styles.inputInvisible}
                    style={{ width: "2em" }}
                  />{" "}
                  MONTHS{" "}
                </div>{" "}
                <span>
                  {" "}
                  <img src={sketch} alt="" /> 3 YEARS{" "}
                </span>{" "}
              </span>
            </div>
          </div>

          <div>
            <hr />
          </div>

          <div className={styles.third}>
            <p className={styles.smOp}>YOU WILL RECIEVE: &nbsp; </p>
            <span>
              {" "}
              19000000.00 <img src={HCF} alt="" /> &nbsp; HCF &nbsp;{" "}
            </span>
            <span className={styles.smOp}>
              FOR 3 YEARS COMMITMENT: 1 HCF = 1HERCULES
            </span>
          </div>
        </div>
        <div className={styles.tab4}>
          <h2>SUMMARY</h2>
          <div>
            <div>
              <p className={styles.smOp}>YOUR EXPECTED RETURNS (YEARLY)</p>
              <p>$ 19,654.65</p>
            </div>

            <div>
              <p className={styles.smOp}>YOUR EXPECTED RETURNS (MONTHLY)</p>
              <p>$ 798.65</p>
            </div>

            <div>
              <p className={styles.smOp}>YOUR EXPECTED APR</p>
              <p>46.56%</p>
            </div>

            <div>
              <p className={styles.smOp}>TREASURY EXPECTED RETURNS (YEARLY)</p>
              <p>$ 1,219,654.65</p>
            </div>

            <div>
              <p className={styles.smOp}>TREASURY EXPECTED RETURNS (MONTHLY)</p>
              <p>$ 165,798.65</p>
            </div>

            <div>
              <p className={styles.smOp}>TOT HCF (YOURS + OTHERS)</p>
              <p>
                $ 19,987,654.54 <img src={HCF} alt="" /> HCF
              </p>
            </div>
          </div>
        </div>
        <div className={styles.tab5}>
          <div className={styles.info}>
            <p
              className={
                graph === "returnsGraph"
                  ? `${styles.smOp} ${styles.bold}`
                  : styles.smOp
              }
              id="returnsGraph"
              onClick={(e) => {
                setGraph(e.target.id);
              }}
            >
              EXPECTED TOT RETURNS
            </p>
            <p
              className={
                graph === "treasuryGraph"
                  ? `${styles.smOp} ${styles.bold}`
                  : styles.smOp
              }
              id="treasuryGraph"
              onClick={(e) => {
                setGraph(e.target.id);
              }}
            >
              FARMED TREASURY
            </p>
            <p
              className={
                graph === "rewardGraph"
                  ? `${styles.smOp} ${styles.bold}`
                  : styles.smOp
              }
              id="rewardGraph"
              onClick={(e) => {
                setGraph(e.target.id);
              }}
            >
              REWARD DISTRIBUTIONS
            </p>
          </div>
          <div className={styles.chart}>
            {graph === "returnsGraph" && <ReturnsGraph />}
            {graph === "treasuryGraph" && <TreasuryGraph />}
            {graph === "rewardGraph" && <RewardGraph />}
          </div>
        </div>
      </div>
      <div className={styles.sec12}>
        <div className={styles.top}>
          <div>
            <input type="text" required placeholder="NAME" />
          </div>
          <div>
            <input type="text" required placeholder="NAME YOUR SIMULATOR" />
          </div>
        </div>
        <button>SAVE YOUR SIMULATION, GET A PERMALINK!</button>
      </div>
    </div>
  );
};

export default StakingSimulator;
