import React, { useCallback, useEffect, useState } from "react";
import find from 'lodash/find';
import Header from "../../components/Header/Header";
import Styles from "./Swap.module.css";

import Form from "../../components/Form/Form";
import FormInput from "../../components/Form/FormInput";
import Footer from "../../components/Footer/Footer";
import Modal from "../../components/Modal/Modal";
import FormExtra from "../../components/Form/FormExtra";

import File from "./../../images/File.png";
import WarningIcon from "./../../images/Warning-1-circle.png";
import ApiOx from '../../utils/0xApi'
import { approveMax } from '../../utils/eth'
import { ethers } from "ethers";
import {
  fetchBalances,
} from '../../helpers/multicall';
import { Timeout, quoteRefreshSeconds } from '../../utils/Timer.js';
import { useWeb3React } from "@web3-react/core";
import poolsConfig from '../../config/constants/pools.json';

const Swap = ({state}) => {
  const defaultTokenSell = {
    address: '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',
    symbol: 'ETH',
    // icon: getTokenImage('eth')
  }

  const defaultTokenBuy = {
    address: '0xe4f726adc8e89c6a6017f01eada77865db22da14',
    // address: '0xc7AD46e0b8a400Bb3C915120d284AafbA8fc4735',
    // symbol: 'DAI',
    symbol: 'BCP',
    // icon: getTokenImage('0xe4f726adc8e89c6a6017f01eada77865db22da14')
  };

  const ZeroEx = '0xdef1c0ded9bec7f1a1670819833240f027b25eff';

  const [showReview, setShowReview] = useState(false);
  const [quote, setQuote] = useState(null);
  const [sellToken, setSellToken] = useState(defaultTokenSell);
  const [buyToken, setBuyToken] = useState(defaultTokenBuy);
  const [approved, setApproved] = useState(false);
  const [listed, setListed] = useState([]);
  const [allowances, setAllowances] = useState({});
  const [balances, setBalances] = useState({});
  // const dropDownItems = ["eth", "BTC", "DOGE", "XRP", "BNB"];
  const [{ from, to }, setFieldValues] = useState({
    from: "",
    to: "",
  });
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const [frozeQuote, setFrozeQuote] = useState(null)
  const [time, setTime] = useState(0)
  const [Timer, setTimer] = useState(new Timeout(30000, () => {
    setFrozeQuote(null);
    fetchQuote(true)
  }, setTime))

  const api = new ApiOx();
  const { account, library } = useWeb3React()

  const changeSlippage = (value) => {
    api.slippage  = value;
    showSlippageSettings = false;
    fetchQuote();
  }

  const approveToken = async () => {
    await approveMax(sellToken.address, ZeroEx, library.getSigner());
    setApproved(true)
    await fetchOnchainData();    
  }

  function onAmountChange() {
    const decimals = sellToken.decimals || 18;
    amount.bn = new BigNumber(amount.label).multipliedBy(10**decimals);
    fetchQuote()
  }

  const checkApproval = async () => {
    if( sellToken.allowance.isEqualTo(0) ) return false;
    if( sellToken.allowance.isGreaterThanOrEqualTo( ethers.parseEther(from.toString()) ) ) return true;
    return false;
  }

  const fetchOnchainData = async () => {
    // Fetch balances, allowance and decimals
    const listedResult = await fetchBalances(
      listed,
      account,
      library.getSigner()
    )
    setListed(listedResult)

    if(sellToken) {
      setSellToken(find(listedResult, ['address', sellToken.address], defaultTokenSell));
    } else {
      setSellToken(defaultTokenSell)
    }

    if(buyToken) {
      setBuyToken(find(listedResult, ['address', buyToken.address], defaultTokenBuy));
    } else {
      setBuyToken(defaultTokenBuy)
    }

    const allowances0 = {}
    const balances0 = {}

    listedResult.forEach( token => {
      allowances0[token.address] = token.allowance;
    })
    setAllowances(allowances0)

    listedResult.forEach( token => {
      balances0[token.address] = token.balance;
    })
    setBalances(balances0)
    console.log("balances:",balances);
  }

  const fetchQuote = async (selfRefresh=false, freeze=false) => {
    if(!from || from === "0" || isLoading === true) {
      // setFieldValues({from: "", to: ""})
      Timer.stop();
      return;
    };

    if(!selfRefresh) {
      setLoading(true);
      // setQuote(null);
      setError(null)
    } else {
      console.log('refreshing quote')
    }
    console.log("buytoken", buyToken);
    const res = await api.getQuote(sellToken, buyToken, ethers.utils.parseEther(from.toString()));
    console.log(res);
    if(account && !approved) {
      setApproved(await checkApproval())
    }
    
    if(res.validationErrors) {
      switch(res.validationErrors[0].code) {
        case 1004:
          setError(`Swap Unavailable.`)
          break;
      }
      setLoading(false);
      return;
    } else if(res.status === 500) {
      setError(`Swap Unavailable.`)
      setLoading(false);
      return;
    }
    setQuote(res);
    setFieldValues({from: from, to: ethers.utils.formatEther(res.buyAmount)});
    setLoading(false);

    Timer.start();

    if(freeze) {
      setFrozeQuote(quote);
    }
  }

  const swap = async () => {
    if(!quote) {
      setError("You need a quote first.")
      return;
    }

    if (!$eth.address || !$eth.signer) {
      displayNotification({ message: $_("piedao.please.connect.wallet"), type: "hint" });
      connectWeb3();
      return;
    }

    const gasPercentagePlus = BigNumber(quote.gas.toString()).multipliedBy(BigNumber(2.1)).toFixed(0);
    const transaction = {
        to: quote.to,
        value: ethers.BigNumber.from(quote.value),
        data: quote.data,
        gasLimit: ethers.BigNumber.from(gasPercentagePlus),
    };

    modal.close();

    const tx = await $eth.signer.sendTransaction(transaction);
    console.log(tx);

    tx.on("txConfirmed", ({ hash }) => {
      // const { dismiss } = displayNotification({
      //   message: "Confirming...",
      //   type: "pending",
      // });

      const subscription = subject("blockNumber").subscribe({
        next: () => {
          // displayNotification({
          //   autoDismiss: 15000,
          //   message: `${amount.label.toFixed(2)} ${sellToken.symbol} swapped successfully`,
          //   type: "success",
          // });
          fetchOnchainData();
          amount = defaultAmount;
          dismiss();
          subscription.unsubscribe();
        },
      });

      return {
        autoDismiss: 1,
        message: "Mined",
        type: "success",
      };
    });
  }

  const handleSubmit = () => {
    if(approved) {
      setShowReview(!showReview);
    }
    else {
      approveToken()
    }
  };

  const setupListedToken = () => {
    const primaryListed = [
      {
        address: '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',
        symbol: 'ETH',
        // icon: getTokenImage('eth')
      },
      {
        address: '0xad32A8e6220741182940c5aBF610bDE99E737b2D',
        symbol: 'DOUGH',
        // icon: getTokenImage('0xad32A8e6220741182940c5aBF610bDE99E737b2D')
      },
      {
        // address: '0x6b175474e89094c44da98b954eedeac495271d0f',
        address: '0xc7AD46e0b8a400Bb3C915120d284AafbA8fc4735',
        symbol: 'DAI',
        // icon: getTokenImage('0x6B175474E89094C44Da98b954EedeAC495271d0F')
      }
    ];
    for (let i = 0; i < poolsConfig.available.length; i++) {
      let pie = poolsConfig[poolsConfig.available[i]];
      if(!pie.useMintOverBuy) {
        primaryListed.push({
          address: poolsConfig.available[i],
          symbol: pie.symbol,
          // icon: getTokenImage(poolsConfig.available[i])
        })
      }
    }
    setListed(primaryListed)
  }

  const tokenSelectCallback = useCallback((targetModal, token) => {
    console.log(token);
    if (token) {
      if(targetModal === 'from') {
        if(token.address === buyToken.address) {
          return;
        }
        setSellToken(token);
      } else if(targetModal === 'to') {
        if(token.address === sellToken.address) {
          return;
        }
        setBuyToken(token);
      }
      console.log(token);
      // fetchQuote();
    }
  }, []);

  useEffect(() => {
    setupListedToken()
    return Timer.stop()
  }, [])

  useEffect(() => {
    if(account) {
      fetchOnchainData();
    }
  }, [account])

  useEffect(() => {
    fetchQuote()
  }, [from, buyToken, sellToken])

  return (
    <div>
      {showReview && (
        <Modal showModal={showReview} closeModal={setShowReview}>
          <div className={Styles.modal}>
            <h2>REVIEW QUOTE</h2>
            <p>Quote expires in 30 seconds</p>

            <div className={Styles.modalContainer}>
              <Form submitText="SWAP">
                <FormInput disabled value={from} leftLabel="Your Pay" />
                <FormInput disabled value={to} leftLabel="You recieve" />
                <FormExtra>
                  <p className={Styles.extra}>
                    Price you pay: <br /> CoinGecko Price: <br /> Guaranteed
                    Price: <br />{" "}
                    <div>
                      <img src={WarningIcon} alt="warning" />
                      <img src={WarningIcon} alt="warning" />
                      <img src={WarningIcon} alt="warning" />
                    </div>
                  </p>
                  <p className={Styles.extra}>
                    1 BCP @0.001168 ETH ($3.88) <br /> $3.12 <br /> 1 RTH @
                    856.478656 BCP <br />
                    <span>Slippage 19.59%</span>
                  </p>
                </FormExtra>
              </Form>
            </div>
          </div>
        </Modal>
      )}
      <div className={`${Styles.sec} `}>
        <section className={Styles.titleSection}>
          <h2>
            <img src={File} alt="" className="title-icon" />
            EXCHANGE TOKEN
          </h2>
          <p>Swap HCF at the best rates</p>
        </section>

        <div className={Styles.container}>
          <Form submitText={account&&approved?"Review Orders":"approve"} extra submit={handleSubmit}>
            <FormInput
              id="from"
              leftLabel="From"
              setField={setFieldValues}
              value={from}
              // dropDown={dropDownItems}
              listed={listed}
              callback={tokenSelectCallback}
            />
            <FormInput
              id="to"
              leftLabel="To(Estimate)"
              setField={setFieldValues}
              value={to}
              // dropDown={dropDownItems}
              listed={listed}
              callback={tokenSelectCallback}
              disabled
            />
            <FormExtra>
              <h5>Max Spillage</h5>
              <h5>3%</h5>
            </FormExtra>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Swap;
